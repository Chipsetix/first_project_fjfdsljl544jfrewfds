<?php

namespace Website\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

//use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Lang
 *
 * @ORM\Table(name="Lang",options={"collate"="utf8_general_ci"})
 * @ORM\Entity(repositoryClass="Website\BackendBundle\Repository\LangRepository")
 */
class Lang
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50, nullable=false )
     */
    private $title;
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=50, nullable=true )
     */
    private $slug;
    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=5,  nullable=false )
     */
    private $locale;

    /**
     * @var string
     *
     * @ORM\Column(name="disable", type="boolean", nullable=false )
     */
    private $disable;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="consecutive_number", type="integer", nullable=false)
     */
    private $con_num;

    /**
     * Construct
     */
    public function __construct()
    {

    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return Lang
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get disable
     *
     * @return boolean
     */
    public function getDisable()
    {
        return $this->disable;
    }

    /**
     * Set disable
     *
     * @param boolean $disable
     * @return Lang
     */
    public function setDisable($disable)
    {
        $this->disable = $disable;

        return $this;
    }

    /**
     * Get con_num
     *
     * @return integer
     */
    public function getConNum()
    {
        return $this->con_num;
    }

    /**
     * Set con_num
     *
     * @param integer $conNum
     * @return Lang
     */
    public function setConNum($conNum)
    {
        $this->con_num = $conNum;

        return $this;
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Lang
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Lang
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }
}
