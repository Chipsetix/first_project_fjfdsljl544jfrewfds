<?php

namespace Website\BackendBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Reviews
 *
 * @ORM\Table(name="Reviews",options={"collate"="utf8_general_ci"})
 * @ORM\Entity(repositoryClass="Website\BackendBundle\Repository\ReviewsRepository")
 */
class Reviews
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserFrontend", inversedBy="allReviews")
     * @ORM\JoinColumn(name="user_front_id", referencedColumnName="id")
     */
    private $userFront;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true )
     */
    private $description;
    /**
     * @var string
     * @ORM\Column(name="videoUrl",type="string",length=100, nullable=true )
     */
    private $videoUrl;
    /**
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_create", type="datetime", nullable=false)
     */
    private $dateCreate;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string",length=5, nullable=true )
     */
    private $type;

    /**
     * Construct
     */
    public function __construct()
    {

        $this->dateCreate = new DateTime('now');
        $this->status = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Reviews
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Reviews
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /* Set dateCreate
    *
    * @param \DateTime $dateCreate
    * @return Reviews
    */

    /**
     * Get dateCreate
     *
     * @return DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get userFront
     *
     * @return UserFrontend
     */
    public function getUserFront()
    {
        return $this->userFront;
    }

    /**
     * Set userFront
     *
     * @param UserFrontend $userFront
     * @return Reviews
     */
    public function setUserFront(UserFrontend $userFront = null)
    {
        $this->userFront = $userFront;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Reviews
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get videoUrl
     *
     * @return string
     */
    public function getVideoUrl()
    {
        return $this->videoUrl;
    }

    /**
     * Set videoUrl
     *
     * @param string $videoUrl
     * @return Reviews
     */
    public function setVideoUrl($videoUrl)
    {
        $this->videoUrl = $videoUrl;

        return $this;
    }

}
