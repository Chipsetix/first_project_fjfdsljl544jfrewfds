<?php

namespace Website\BackendBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Website\BackendBundle\Repository\DopageRepository")
 * @ORM\Table(name="Dopage", options={"collate"="utf8_general_ci"})
 * @ORM\HasLifecycleCallbacks
 */
class Dopage
{

    /**
     * @var string
     *
     * @ORM\Column(name="titlePathFirst", type="string", length=80, nullable=false)
     */
    public $titlePathFirst;
    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=30, nullable=false)
     */
    public $url;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;


    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=5, nullable=false )
     */
    private $locale;

    /**
     * @ORM\ManyToOne(targetEntity="Lang")
     * @ORM\JoinColumn(name="lang_id", referencedColumnName="id", nullable=true)
     */
    private $lang;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_create", type="date", nullable=false)
     */
    private $dateCreate;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_modify", type="date", nullable=false)
     */
    private $dateModify;


    public function __construct()
    {
        $this->dateCreate = new DateTime('now');
        $this->dateModify = new DateTime('now');
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get titlePathFirst
     *
     * @return string
     */
    public function getTitlePathFirst()
    {
        return $this->titlePathFirst;
    }

    /**
     * Set titlePathFirst
     *
     * @param string $titlePathFirst
     *
     * @return Dopage
     */
    public function setTitlePathFirst($titlePathFirst)
    {
        $this->titlePathFirst = $titlePathFirst;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Dopage
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Dopage
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set locale
     *
     * @param string $locale
     *
     * @return Dopage
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateCreate
     *
     * @param DateTime $dateCreate
     *
     * @return Dopage
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateModify
     *
     * @return DateTime
     */
    public function getDateModify()
    {
        return $this->dateModify;
    }

    /**
     * Set dateModify
     *
     * @param DateTime $dateModify
     *
     * @return Dopage
     */
    public function setDateModify($dateModify)
    {
        $this->dateModify = $dateModify;

        return $this;
    }

    /**
     * Get lang
     *
     * @return Lang
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set lang
     *
     * @param Lang $lang
     *
     * @return Dopage
     */
    public function setLang(Lang $lang = null)
    {
        $this->setLocale($lang->getLocale());

        $this->lang = $lang;

        return $this;
    }
}
