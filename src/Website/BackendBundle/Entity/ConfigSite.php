<?php

namespace Website\BackendBundle\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="Website\BackendBundle\Repository\ConfigSiteRepository")
 * @ORM\Table(name="ConfigSite",options={"collate"="utf8_general_ci"})
 */
class ConfigSite
{

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="page", type="string", length=20, nullable=false)
     */
    public $page;

    /**
     * @var string
     *
     * @ORM\Column(name="base_dir", type="string", length=100, nullable=true )
     */
    private $baseDir;

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=50, nullable=true )
     */
    private $domain;

    /**
     * @var integer
     *
     * @ORM\Column(name="countries", type="integer", length=3, nullable=true)
     */
    private $countries;

    /**
     * @var integer
     *
     * @ORM\Column(name="users", type="integer", length=6, nullable=true)
     */
    private $users;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->page = 'config-site';
        $this->countries = 0;
        $this->users = 0;

    }

    /**
     * Get page
     *
     * @return string
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set page
     *
     * @param string $page
     * @return ConfigSite
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get baseDir
     *
     * @return string
     */
    public function getBaseDir()
    {
        return $this->baseDir;
    }

    /**
     * Set baseDir
     *
     * @param string $baseDir
     * @return ConfigSite
     */
    public function setBaseDir($baseDir)
    {
        $this->baseDir = $baseDir;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return ConfigSite
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * @param int $countries
     */
    public function setCountries($countries)
    {
        $this->countries = $countries;
    }

    /**
     * @return int
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param int $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }


}
