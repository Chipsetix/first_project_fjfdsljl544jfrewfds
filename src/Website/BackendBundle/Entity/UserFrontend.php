<?php

namespace Website\BackendBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="user_front")
 * @ORM\Entity (repositoryClass="Website\BackendBundle\Repository\UserFrontRepository")
 */
class UserFrontend extends User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="user_photo", type="string", length=120, nullable=true)
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="date_create", type="datetime", nullable=false)
     */
    private $dateCreate;


    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true )
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="comment2", type="text", nullable=true )
     */
    private $comment2;

    /**
     * @var integer
     *
     * @ORM\Column(name="tries", type="integer", length=1, nullable=false )
     */
    private $tries;

    /**
     * @ORM\OneToMany(targetEntity="UserFrontend", mappedBy="upliner")
     */
    private $children;

    /**
     * @var integer
     *
     * @ORM\Column(name="confirmation_pin", type="text", nullable=true )
     */
    private $confirmationPin;

    /**
     * @var string
     *
     * @ORM\Column(name="double_auth", type="boolean", nullable=false )
     */
    private $doubleAuth;

    /**
     * @ORM\OneToMany(targetEntity="Reviews", mappedBy="userFront")
     */
    private $allReviews;

//----------------------------------------------------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();

        $this->dateCreate = new DateTime('now');
        $this->tries = 0;
        $this->doubleAuth = false;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return UserFrontend
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateCreate
     *
     * @param DateTime $dateCreate
     *
     * @return UserFrontend
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return UserFrontend
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment2
     *
     * @return string
     */
    public function getComment2()
    {
        return $this->comment2;
    }

    /**
     * Set comment2
     *
     * @param string $comment2
     *
     * @return UserFrontend
     */
    public function setComment2($comment2)
    {
        $this->comment2 = $comment2;

        return $this;
    }

    /**
     * Add child
     *
     * @param UserFrontend $child
     *
     * @return UserFrontend
     */
    public function addChild(UserFrontend $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param UserFrontend $child
     */
    public function removeChild(UserFrontend $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add allReview
     *
     * @param Reviews $allReview
     *
     * @return UserFrontend
     */
    public function addAllReview(Reviews $allReview)
    {
        $this->allReviews[] = $allReview;

        return $this;
    }

    /**
     * Remove allReview
     *
     * @param Reviews $allReview
     */
    public function removeAllReview(Reviews $allReview)
    {
        $this->allReviews->removeElement($allReview);
    }

    /**
     * Get allReviews
     *
     * @return Collection
     */
    public function getAllReviews()
    {
        return $this->allReviews;
    }

    /**
     * Get tries
     *
     * @return integer
     */
    public function getTries()
    {
        return $this->tries;
    }

    /**
     * Set tries
     *
     * @param integer $tries
     *
     * @return UserFrontend
     */
    public function setTries($tries)
    {
        $this->tries = $tries;

        return $this;
    }

    /**
     * Get confirmationPin
     *
     * @return string
     */
    public function getConfirmationPin()
    {
        return $this->confirmationPin;
    }

    /**
     * Set confirmationPin
     *
     * @param string $confirmationPin
     *
     * @return UserFrontend
     */
    public function setConfirmationPin($confirmationPin)
    {
        $this->confirmationPin = $confirmationPin;

        return $this;
    }

    /**
     * Get doubleAuth
     *
     * @return boolean
     */
    public function getDoubleAuth()
    {
        return $this->doubleAuth;
    }

    /**
     * Set doubleAuth
     *
     * @param boolean $doubleAuth
     *
     * @return UserFrontend
     */
    public function setDoubleAuth($doubleAuth)
    {
        $this->doubleAuth = $doubleAuth;

        return $this;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return UserFrontend
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }
}
