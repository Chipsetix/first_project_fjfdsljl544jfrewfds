<?php

namespace Website\BackendBundle;

/**
 * Contains all events thrown in the FOSUserBundle.
 */
final class Events
{

    const DOUBLE_AUTH = 'doubleAuth';

    const SESSION_FLASH_SUCCESS = 'flash.success';

    const SESSION_FLASH_ERROR = 'flash.error';

    const SEND_EMAIL = 'send.mail';
}
