<?php

namespace Website\BackendBundle\Model;

class Data
{
    /**Попыток входа*/
    public static $tries = 5;

    /**Отправка почты true or false*/
    public static $sendMail = [
        'registration' => true,
        'status' => false,
        'feedback_email' => true,
        'doubleAuth' => false,
        'authorize_code_email' => true
    ];

    /**Кол-во отображения на страницу админка*/
    public static $limit_page_admin = 100;

    /**Кол-во отображения на страницу гостевая*/
    public static $limit_page = 10;

    public static function arrayCombinate($key, $value)
    {
        return array_combine($key, $value);
    }

    public static function offset($page, $limit)
    {
        if ($page == 1) {
            $offset = 0;
        } else {
            $offset = ($page * $limit) - $limit;
        }
        return $offset;
    }

    public static function WebDir()
    {
        return realpath(__DIR__ . '/../../../../web');
    }
}
