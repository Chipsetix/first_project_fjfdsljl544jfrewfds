<?php

namespace Website\BackendBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

Class Composite
{
    /**
     * @var EntityManagerSugar $EMS
     */
    private $_EntityManagerSugar;

    /**
     * @var FormSugar $FS
     */
    private $_FormSugar;

    /**
     * @var SupportClass $Support
     */
    private $_Support;

    public function __construct(EntityManagerSugar $EMS, FormSugar $FS, SupportClass $Support)
    {
        $this->_EntityManagerSugar = $EMS;
        $this->_FormSugar = $FS;
        $this->_Support = $Support;
    }

    /**
     * Sets table in EntityManagerSugar for queries
     *
     * @param string $table
     *
     * @return $this
     */
    public function setTable($table)
    {
        $this->getEntityManagerSugar()->setTableName($table);

        return $this;
    }

    /**
     * Returns instans of EntityManagerSugar
     *
     * @return EntityManagerSugar
     */
    public function getEntityManagerSugar()
    {
        return $this->_EntityManagerSugar;
    }

    /**
     * Show all elements
     *
     * @return array
     */
    public function ShowAll($is_pagination = null, $page = null)
    {
        return array(
            'entities' => $this->getEntityManagerSugar()->GetAll($is_pagination, $page),
        );
    }

    /**
     * Show one element by $parameter array
     *
     * @param Array $param
     *
     * @return Array
     */
    public function ShowBy($param)
    {
        return array(
            'entity' => $this->getEntityManagerSugar()->findOneBy($param),
        );
    }

    /**
     * Create News and redirect or show Create page
     *
     * @param Request $request
     *
     * @return Array
     */
    public function CreateElement(Request $Request, $Entity, $FormType, $Redirect, $need_id = true)
    {
        $Form = $this
            ->getFormSugar()
            ->createForm(
                $FormType,
                $Entity
            );

        $Form->handleRequest($Request);


        if (!$Form->isValid()) {
            $this->FlashBag('notice', 'Ошибка ввода!');
            return $this->Novel($Entity, $FormType);

        } else {

            $this
                ->getEntityManagerSugar()
                ->PersistAndFlush($Entity);

            $this->FlashBag('success', 'Запись успешно cохранена!');
        }

        return
            $this->getSupport()
                ->MakeRedirect(
                    $Redirect,
                    ($need_id)
                        ? array('id' => $Entity->getId())
                        : NULL
                );

    }

    /**
     * Returns instans of FormSugar
     *
     * @return FormSugar
     */
    public function getFormSugar()
    {
        return $this->_FormSugar;
    }

    public function FlashBag($type, $mes = 'Запись успешно cохранена!')
    {
        return $this
            ->getSupport()
            ->FlashBag(
                $type,
                $mes
            );
    }

    /**
     * Returns instans of SupportClass
     *
     * @return SupportClass
     */
    public function getSupport()
    {
        return $this->_Support;
    }

    /**
     * Return Array to show the new entry
     *
     * @param Object $Entity
     * @param $FormType
     * @return Array
     */
    public function Novel($Entity, $FormType, $param = false)
    {
        $arr_param = ['method' => 'POST'];

        if ($param) {
            $arr_param['currency'] = $param;
        }

        $Form = $this
            ->getFormSugar()
            ->createForm(
                $FormType,
                $Entity,
                $arr_param
            );

        return array(
            'entity' => $Entity,
            'form' => $Form->createView(),
        );

    }

    /**
     * Show one element by $id
     *
     * @param int $id
     *
     * @return Array
     */
    public function Show($id)
    {
        return array(
            'entity' => $this->getEntityManagerSugar()->findOne($id),
            'delete_form' => $this->getFormSugar()->createDeleteForm($id)->createView(),
        );

    }

    /**
     * Get One Entity by id and create forms for edit and for delete
     *
     * @param mix $param
     * @param $Form
     *
     * @return Array
     */
    public function Edit($param, $Form)
    {
        $Entity = NULL;

        if ('array' == gettype($param)) {
            $Entity = $this
                ->getEntityManagerSugar()
                ->findOneBy($param);
        } else {
            $Entity = $this->getEntityManagerSugar()->findOne($param);
        }

        return array(
            'entity' => $Entity,
            'edit_form' => $this->getFormSugar()->createEditForm($Form, $Entity)->createView(),
            'delete_form' => $this->getFormSugar()->createDeleteForm($param)->createView(),
        );
    }

    /**
     * Update entity and redirect
     *
     * @param Request $request
     * @param int $id
     * @param $FormType
     * @param string $RouteName
     * @param $RouteParam
     *
     * @return redirect to $RouteName with params $RouteParam
     */
    public function Update(Request $request, $id, $FormType, $RouteName, $RouteParam = null)
    {
        $Entity = $this->getEntityManagerSugar()->findOne($id);

        $this->WriteElement($request, $Entity, $FormType);

        return $this
            ->getSupport()
            ->MakeRedirect($RouteName, $RouteParam);

    }

    public function WriteElement(Request $Request, $Entity, $FormType)
    {
        $Form = $this
            ->getFormSugar()
            ->createEditForm($FormType, $Entity);

        $Form->handleRequest($Request);

        if ($Form->isSubmitted() && $Form->isValid()) {
            $this
                ->getEntityManagerSugar()
                ->PersistAndFlush($Entity);

            $this->FlashBag('success', 'Запись успешно cохранена!');


            return $Form;
        } else {

            $this->FlashBag('notice', 'Изменения не сохранены! была допущена ошибка');

        }

        return $this;

    }

    /**
     * Delete nomenklature element
     *
     * @param Request $Request
     * @param int $id
     * @param string $RouteName
     * @param string $RouteParam
     * @return redirect
     */
    public function DeleteParam(Request $Request, $id, $RouteName, $RouteParam = NULL)
    {

        return $this->Delete($Request, $id, $RouteName, $RouteParam);

        //return $this->MakeRedirect($RouteName,$RouteParam);

    }

    /**
     * Delete element andredirect
     *
     * @param Request $request
     * @param int id
     * @param string $RouteName
     * @param Array $RouteParam
     *
     * @return redirect to $RouteName with params $RouteParam
     */
    public function Delete(Request $Request, $id, $RouteName, $RouteParam = NULL)
    {
        return $this
            ->DeleteElemnt($Request, $id)
            ->getSupport()
            ->MakeRedirect($RouteName, $RouteParam);
    }

    /**
     * Delete element from DB
     *
     * @param Request $Request
     * @param int $id
     *
     * @return self
     */
    public function DeleteElemnt(Request $Request, $id)
    {
        $Form = $this->getFormSugar()->createDeleteForm($id);

        $Form->handleRequest($Request);

        if ($Form->isValid()) {
            $Entity = $this->getEntityManagerSugar()->findOne($id);

            if (empty($Entity)) {
                throw $this->getSupport()->createNotFoundException('No Entity find!');
            }

            $this
                ->getEntityManagerSugar()
                ->RemoveAndFlush($Entity);

            $this->FlashBag('success', 'Запись удалена успешно!');
        }

        return $this;
    }

    /**
     *
     * @param $Request
     * @param $Entity
     * @param $FormType
     *
     * @return response
     */
    public function ErrorElement(Request $Request, $Entity, $FormType)
    {
        $Form = $this
            ->getFormSugar()
            ->createForm(
                $FormType,
                $Entity
            );

        $Form->handleRequest($Request);

        return $Form;

    }


}