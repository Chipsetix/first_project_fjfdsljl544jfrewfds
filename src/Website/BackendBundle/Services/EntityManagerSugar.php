<?php

namespace Website\BackendBundle\Services;

use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Paginator;

Class EntityManagerSugar
{

    /**
     * Entity manager
     * @var Object insans of EntityManager
     */
    private $_EntityManager;

    /**
     * Table name
     *
     * @var string
     */
    private $_table_name;

    /**
     * Paginator
     *
     * @var Object
     */
    private $_Paginator;

    /**
     * Rows per page
     *
     * @var int
     */
    private $_limit_per_page;


    /**
     * @param EntityManager $EM
     */
    public function __construct(EntityManager $EM, Paginator $Paginator, $per_page)
    {
        $this->_EntityManager = $EM;
        $this->_Paginator = $Paginator;

        $this->_limit_per_page = $per_page;

    }

    /**
     * Returns Paginator
     *
     * @return Paginator
     */
    public function getPaginator()
    {
        return $this->_Paginator;
    }

    /**
     * Returns Support class
     *
     * @return SupportClass
     */
    public function getSupport()
    {
        return $this->_SupportClass;
    }

    public function getQueryBuilder()
    {
        return $this->getEntityManager()->createQueryBuilder();
    }

    /**
     * Returns instans of EntityManager
     *
     * @return Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->_EntityManager;
    }

    /**
     * Get reposiry by table name
     *
     * @param string $tableName
     *
     * @return Doctrine\ORM\EntityRepository
     */
    public function getRepository($tableName)
    {
        return $this->getEntityManager()->getRepository($tableName);
    }

    /**
     * Persist and Flush income $Object
     *
     * @param Object $Object
     *
     * @return self
     */
    public function PersistAndFlush($Object)
    {

        $this->getEntityManager()->persist($Object);

        $this->getEntityManager()->flush();

        return $this;
    }

    /**
     * Persist $Object
     *
     * @param Object $Object
     *
     * @return self
     */
    public function Persist($Object)
    {

        $this->getEntityManager()->persist($Object);
        return $this;
    }

    /**
     * Flush income $Object
     *
     * @param Object $Object
     *
     * @return self
     */
    public function Flush()
    {

        $this->getEntityManager()->flush();

    }

    /**
     * Remove and Flush income $Object
     *
     * @param Object $Object
     *
     * @return self
     */
    public function RemoveAndFlush($Object)
    {

        $this->getEntityManager()->remove($Object);

        $this->getEntityManager()->flush();

        return $this;
    }

    /**
     * Return one element of Entity or trow exeption
     *
     * @param int $id item id
     * @param string $error_text Text will display, If no entity find
     *
     * @return Object instans of curent tabel Entity
     */
    public function findOne($id)
    {
        return $this->getOneElementById($id, $this->getTableName());
    }

    /**
     * Return one element of Menu Entity
     *
     * @param int $id
     * @param string $EntityName exeption text
     *
     * @return Object current entity instans
     */
    public function getOneElementById($id, $table_name)
    {
        return $this
            ->getEntityManager()
            ->getRepository($table_name)
            ->findOneById($id);
    }

    /**
     * Returns string with entity path:name
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->_table_name;
    }

    /**
     * Set $_table_name and returns themself
     *
     * @return Object returns themself
     */
    public function setTableName($table_name)
    {
        $this->_table_name = $table_name;
        return $this;
    }

    /**
     * Return one element of Entity or trow exeption
     *
     * @param int $id item id
     * @param string $error_text Text will display, If no entity find
     *
     * @return Object instans of curent tabel Entity
     */
    public function findOneBy($param)
    {
        return $this->getOneElementBy($param, $this->getTableName());
    }

    /**
     * Return one element of Menu Entity by input array
     *
     * @param Array $Params
     * @param string $EntityName exeption text
     *
     * @return Object current entity instans
     */
    public function getOneElementBy($Params, $table_name)
    {
        return $this
            ->getEntityManager()
            ->getRepository($table_name)
            ->findOneBy($Params);
    }

    /**
     * Returns all rows with pagination if $paginator is not null or without - if null
     *
     * @param Paginator $paginator
     * @param int page
     * @param
     */
    public function GetAll($paginator = null, $page = null, $filter = null)
    {

        # подумать как убрать пагинатор и убрать ли, возможно привыборе в на уровень выше
        # сделать выбор между пагинатором и обычной моделью. через интерфейсы
        if (!empty($paginator)) {
            if (empty($page)) {
                $page = 1;
            }

            return $this->GetAllWithPagination($page);
        }

        return $this->findAll();
    }

    /**
     * Retrns instans of \Knp\Component\Pager\Paginator including the required rows
     *
     * @param int $page
     *
     * @return Array of Obects
     */
    public function GetAllWithPagination($page)
    {
        return $this
            ->_Paginator
            ->paginate(
                $this->getDQLQuery($this->DQLgetAll()),
                $page,
                $this->getLimitPerPage()
            );
    }

    /**
     * Returns instans of Doctrine\ORM\Query, Object with query
     *
     * @param string $query
     *
     * @return Doctrine\ORM\Query
     */
    public function getDQLQuery($query)
    {
        return $this->getEntityManager()->createQuery($query);
    }

    /**
     * Returns DQL sting
     *
     * @return string
     */
    private function DQLgetAll()
    {
        return 'SELECT n FROM ' . $this->getTableName() . ' n order By n.id DESC';
    }

    /**
     * Returns int - Rows per page
     */
    public function getLimitPerPage()
    {
        return $this->_limit_per_page;
    }

    /**
     * Returns int - Rows per page
     */
    public function setLimitPerPage($limit)
    {
        $this->_limit_per_page = $limit;
        return $this;
    }

    /**
     * Return all rows from database as array of Entities
     *
     * @return Array of Entities
     */
    public function findAll()
    {
        $Result = $this
            ->getEntityManager()
            ->getRepository($this->getTableName())
            ->findAll();

        return (array_reverse($Result));
    }


}