<?php

namespace Website\BackendBundle\Services;

use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Paginator;
use Swift_Message;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Validator\Constraints\Date;
use Website\BackendBundle\Event\EmailEvent;
use Website\BackendBundle\Event\FlashEvent;
use Website\BackendBundle\Events;
use Website\BackendBundle\Model\Data;

Class HelperClass
{
    /**
     * Entity manager
     * @var Object insans of EntityManager
     */
    private $_EntityManager;

    /**
     * Paginator
     *
     * @var Object
     */
    private $_Paginator;

    private $kernel;
    private $templating;

    private $_EventDispatcher;

    public function __construct(EntityManager $em, Paginator $Paginator, $kernel, $Mailer, $templating, $EventDispatcher)
    {
        $this->_EntityManager = $em;

        $this->_Paginator = $Paginator;

        $this->kernel = $kernel;

        $this->templating = $templating;
        $this->_Mailer = $Mailer;
        $this->_EventDispatcher = $EventDispatcher;
    }

    public function getOperationsClass()
    {
        return $this->_Operations_class;
    }

    /**
     * Returns Paginator
     *
     * @return Paginator
     */
    public function getPaginator()
    {
        return $this->_Paginator;
    }

    public function getKernel()
    {
        return $this->kernel;
    }

    public function NewLangCoocies($request, $locale)
    {
        $configSite = $this->getEntityManager()->getRepository('WebsiteBackendBundle:ConfigSite')->findOneByPage('config-site');
        $lang = $this->getEntityManager()->getRepository('WebsiteBackendBundle:Lang')->findOneByLocale($locale);

        return new Cookie('locale', $request->getLocale(), time() + 3600 * 24 * 3, '/', $configSite->getDomain(), 0, 0);
    }

    /**
     * Returns instans of EntityManager
     *
     * @return Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->_EntityManager;
    }

    public function NewCoocies($response, $UplinerName)
    {

        $configSite = $this->getEntityManager()->getRepository('WebsiteBackendBundle:ConfigSite')->findOneByPage('config-site');

        $Cookie = new Cookie("websitecookie", $UplinerName->getUsername(), time() + 3600 * 24 * 3, "/", 'nika_nika.com', 1);

        return $Cookie;
    }

    public function getUpliner($cookies, $upLinerRef = null)
    {

        /*Проверяем существет ли $cookies->has('websitecookie')*/
        if ($cookies->has('websitecookie')) {
            /*Записываем аплайнера $UplinerName*/
//            $UplinerName =$this->getUserName($cookies->get('websitecookie'));
            $UplinerName = $this
                ->getEntityManager()
                ->getRepository('WebsiteBackendBundle:UserFrontend')
                ->findOneByUsername($cookies->get('websitecookie'));

            $upLinerRef = $UplinerName->getReferUrl();
        }

        return $this->AddUpliner($upLinerRef);

    }

    public function currencyArrayMap($Array)
    {
        $arrResult = [];
        array_map(function ($item) use (&$arrResult) {

            $arrResult[$item['currency']] = (float)$item['param'];

        }, $Array);

        foreach (Data::$arrCurrency as $cur) {
            if (array_key_exists($cur, $arrResult)) {
                $arrResult[$cur] = $arrResult[$cur];
            } else {
                $arrResult += [$cur => 0];
            }
        }

        return $arrResult;
    }

    public function paginator($result, $page, $limit = 'base')
    {
        return $this->_Paginator->paginate($result, $page, ((int)$limit) ? $limit : Data::$limit_page_admin);
    }

    public function sendMail($From, $To, $Subject, $param, $template)
    {

        $message = Swift_Message::newInstance();

        $message
            ->setSubject($Subject)
            ->setFrom($From)
            ->setTo($To)
            ->setBody(
                $this->templating->render(
                    'WebsiteFrontendBundle:Mail:' . $template . '.html.twig', $param
                ), 'text/html'
            );

        $this->_Mailer->send($message);

    }

    public function BaseDir()
    {
        return $this->kernel->getProjectDir() . DIRECTORY_SEPARATOR;
    }

    public function Success($constant)
    {
        return ['message' => $constant];
    }

    /**
     *
     * @param date $date
     * @param integer $day
     * @param string $format
     *
     * @return int
     */
    public function dateModify($date, $day, $format = 'Y-m-d')
    {
        return date_format(date_modify($date, '+' . $day . ' day'), $format);
    }

    /**Dispatch**/

    public function dispatchSendEmail($params)
    {
        if (Data::$sendMail[$params['template']] == true) {

            $this->getEventDispatcher()->dispatch(Events::SEND_EMAIL, new EmailEvent($params));
        }
    }

    public function getEventDispatcher()
    {
        return $this->_EventDispatcher;
    }

    public function dispatchErrorFlash($translator)
    {
        $this->getEventDispatcher()->dispatch(Events::SESSION_FLASH_ERROR, new FlashEvent($translator));
    }

    public function dispatchSuccessFlash($translator)
    {
        $this->getEventDispatcher()->dispatch(Events::SESSION_FLASH_SUCCESS, new FlashEvent($translator));
    }
}