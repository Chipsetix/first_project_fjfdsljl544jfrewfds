<?php

namespace Website\BackendBundle\Services;


use Exception;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

//use Symfony\Component\Security\Core\SecurityContext;
//use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

Class SupportClass
{

    /**
     * @var Object Request
     */
    #private $_Request;


    /**
     * @var Object Router
     */
    private $_Router;


    /**
     * @var Object ContainerInterface
     */
    private $_Container;

    /**
     * @var Object SecurityContext
     */
    private $_SecurityContext;

    /**
     * @var Object Session
     */
    private $_Session;

    /**
     * @param Request $Request
     * @param Router $Router
     */
    public function __construct(Router $Router, ContainerInterface $Container, AuthorizationChecker $AuthorizationChecker, Session $Session)
    {
        $this->_Router = $Router;
        $this->_Container = $Container;
        $this->_AuthorizationChecker = $AuthorizationChecker;
        $this->_Session = $Session;
    }

    public function getAuthorizationChecker()
    {
        return $this->_AuthorizationChecker;
    }

    /**
     * Returns a NotFoundHttpException.
     *
     * This will result in a 404 response code. Usage example:
     *
     *     throw $this->createNotFoundException('Page not found!');
     *
     * @param string $message A message
     * @param Exception $previous The previous exception
     *
     * @return NotFoundHttpException
     */
    public function createNotFoundException($message = 'Not Found', Exception $previous = null)
    {
        return new NotFoundHttpException($message, $previous);
    }

    /**
     * Make redirect in right direction
     *
     * @param string $route Route name from route config file
     * @param array $params
     *
     * @return Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function MakeRedirect($route, $params)
    {
        return $this->redirect(
            $this->CreateUrl($route, $params)
        );
    }

    /**
     * Returns a RedirectResponse to the given URL.
     *
     * @param string $url The URL to redirect to
     * @param integer $status The status code to use for the Response
     *
     * @return RedirectResponse
     */
    public function redirect($url, $status = 302)
    {
        return new RedirectResponse($url, $status);
    }

    /**
     * Create url for redirect
     *
     * @param string $route Route name from route config file
     * @param array $params
     *
     * @return string The generated URL
     *
     */
    public function CreateUrl($route, $params)
    {

        if (empty($params)) {
            return $this->generateUrl($route);
        }

        return $this->generateUrl($route, $params);
    }

    /**
     * Generates a URL from the given parameters.
     *
     * @param string $route The name of the route
     * @param mixed $parameters An array of parameters
     * @param Boolean|string $referenceType The type of reference (one of the constants in UrlGeneratorInterface)
     *
     * @return string The generated URL
     *
     * @see UrlGeneratorInterface
     */
    public function generateUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->getRouter()->generate($route, $parameters, $referenceType);
    }

    /**
     * Get Router Object
     *
     * @return Router
     */
    public function getRouter()
    {
        return $this->_Router;
    }

    /**
     * Returns a rendered view.
     *
     * @param string $view The view name
     * @param array $parameters An array of parameters to pass to the view
     *
     * @return string The rendered view
     */
    public function renderView($view, array $parameters = array())
    {
        return $this->getContainer()->get('templating')->render($view, $parameters);
    }

    public function getContainer()
    {
        return $this->_Container;
    }

    /**
     * Renders a view.
     *
     * @param string $view The view name
     * @param array $parameters An array of parameters to pass to the view
     * @param Response $response A response instance
     *
     * @return Response A Response instance
     */
    public function render($view, array $parameters = array(), Response $response = null)
    {
        return $this->getContainer()->get('templating')->renderResponse($view, $parameters, $response);
    }

    /**
     * @return Factories\UserBundle\Entity\User
     */
    public function GetCurrentUser()
    {
        // return $this->getSecurityContext()->getToken()->getUser();
        return $this->isGranted();
    }

    /**
     * Create message for Create,update,delete
     *
     * @param string $type type message (notice, success..)
     * @param string|array $mes
     *
     * @return array
     */
    public function FlashBag($type, $mes)
    {
        if (!empty($mes)) {
            return $this->setFlash($type, $mes);

        }

    }

    /**
     * Registers a message for a given type.
     *
     * @param string $type type message (notice, success..)
     * @param string|array $mes
     *
     * @return null
     */
    public function setFlash($type, $mes)
    {
        return $this->getSession()->getFlashBag()->set($type, $mes);
    }

    /**
     * Get Session Object
     *
     * @return Session
     */
    public function getSession()
    {
        return $this->_Session;
    }

    /**
     * Gets and clears flash from the stack.
     *
     * @param string $type type message (notice, success..)
     * @param array $mes Default value if $type does not exist.
     *
     * @return array
     */
    public function getFlash($type, $mes)
    {
        return $this->getSession()->getFlashBag()->get($type, array($mes));
    }

    /**
     * Has flash messages for a given type?
     *
     * @param string $type type message (notice, success..)
     *
     * @return boolean
     */
    public function hasFlash($type)
    {
        return $this->getSession()->getFlashBag()->has($type);
    }
}