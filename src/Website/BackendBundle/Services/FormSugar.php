<?php

namespace Website\BackendBundle\Services;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;

Class FormSugar
{

    private $_FormFactory;


    public function __construct(FormFactory $FormFactory)
    {
        $this->_FormFactory = $FormFactory;
    }

    /**
     * Creates a form to delete a Menu entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Form The form
     */
    public function createDeleteForm($id)
    {
        return $this
            ->createFormBuilder()
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Creates and returns a form builder instance
     *
     * @param mixed $data The initial data for the form
     * @param array $options Options for the form
     *
     * @return FormBuilder
     */
    public function createFormBuilder($data = null, array $options = array())
    {
        /*old 2.8*/
        //return $this->_FormFactory->createBuilder( 'form', $data, $options );

        return $this->_FormFactory->createBuilder('Symfony\Component\Form\Extension\Core\Type\FormType', $data, $options);
    }

    /**
     * Create View for edit form
     *
     * @param string $Form
     * @param Object $id current element id
     *
     * @return FormView The view
     */
    public function createEditForm($Form, $Entity)
    {
        return $this
            ->createForm(
                $Form,
                $Entity,
                array('method' => 'PUT')
            );
    }

    /**
     * Creates and returns a Form instance from the type of the form.
     *
     * @param string $type The built type of the form
     * @param mixed $data The initial data for the form
     * @param array $options Options for the form
     *
     * @return Form
     */
    public function createForm($type, $data = null, array $options = array())
    {
        return $this->_FormFactory->create($type, $data, $options);
    }

}