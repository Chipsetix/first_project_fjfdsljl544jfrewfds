<?php

namespace Website\BackendBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class FlashEvent extends Event
{
    private $flash;

    public function __construct($flash)
    {
        $this->flash = $flash;
    }

    public function getFlash()
    {
        return $this->flash;
    }
}