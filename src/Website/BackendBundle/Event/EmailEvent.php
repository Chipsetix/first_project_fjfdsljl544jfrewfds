<?php

namespace Website\BackendBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class EmailEvent extends Event
{
    private $param;

    public function __construct($param)
    {
        $this->param = $param;
    }

    public function getParam()
    {
        return $this->param;
    }
}