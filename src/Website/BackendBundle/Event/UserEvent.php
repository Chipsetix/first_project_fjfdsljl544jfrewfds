<?php

namespace Website\BackendBundle\Event;

use Website\BackendBundle\Entity\User;

class UserEvent
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
}