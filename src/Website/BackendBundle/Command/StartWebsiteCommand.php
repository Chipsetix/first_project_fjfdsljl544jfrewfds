<?php

namespace Website\BackendBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class StartWebsiteCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('website:create-base')
            // the short description shown while running "php bin/console list"
            ->setDescription('Creates a new config-site.')
            // the "--help" option
            ->setHelp('Это команда создает концигурацию сайта')
            ->addArgument('domen', InputArgument::REQUIRED, 'Укажите адрес домена')
            ->addArgument('last_name', InputArgument::OPTIONAL, 'Your last name?')
            ->addOption(
                'lang',
                'l',
                InputOption::VALUE_REQUIRED,
                'Какой язык создать по умолчанию ru или en',
                'ru'
            );;
        // Пример разворачивания на сервере
        //php bin/console website:create-base skeleton.com -l en -e prod

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //получаем сервис, который будет непосредственно добавлять ежедневные бонусы
        $service = $this->getContainer()->get('website_backend.startwebsite');
//        $input->getOption('lang');
        $service->create($input->getArgument('domen'), $input->getOption('lang'));
    }


}