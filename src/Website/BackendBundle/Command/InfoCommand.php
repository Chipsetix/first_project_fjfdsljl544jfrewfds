<?php

namespace Website\BackendBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InfoCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            // the name of the command (the part after "app/console")
            ->setName('website:info:start')
            // the short description shown while running "php app/console list"
            ->setDescription('Обновление, курсов, а так же рассылка ');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $holiday = $em->getRepository('WebsiteBackendBundle:Holidays')->findByOpenByTimeforEmail(date('Y-m-d H:i:s'));

        /**получаем сервис, который будет непосредственно добавлять ежедневные бонусы*/
        $service = $this->getContainer()->get('website_backend.info');

        if ($holiday) {
            $service->sendEmail($holiday);
        }

    }


}