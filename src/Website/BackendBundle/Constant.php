<?php

namespace Website\BackendBundle;

final class Constant
{
    const NOT_VALID = 'message.danger.formNotValid';

    const BLOCK_IO_ADDRESS = 'message.danger.address';

    const BLOCK_IO_LIMIT_ADDRESS = 'message.danger.limit_address';

    const DANGER_PIN = 'message.danger.pin';

    const DANGER_SECRET_WORD = 'message.danger.secretword';

    const NO_VALID_CODE = 'message.danger.novalide_code';


    const SUCCESS_UPDATE_USER = 'message.success.userUpdate';

    const SUCCESS_REVIEWS_SAVED = 'message.success.reviewssaved';

    const SUCCESS_EMAIL = 'message.success.success_email';

    /*****TABLE ORM*****/

    const TABLE_REVIEWS = 'WebsiteBackendBundle:Reviews';

    const TABLE_USER_FRONTEND = 'WebsiteBackendBundle:UserFrontend';
}
