<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Website\BackendBundle\Listener;

use Swift_Message;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Website\BackendBundle\Events;

class EmailListener implements EventSubscriberInterface
{
    private $container;
    private $templating;

    public function __construct(ContainerInterface $container, $mailer, $templating)
    {
        $this->container = $container;
        $this->templating = $templating;
        $this->_mailer = $mailer;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            Events::SEND_EMAIL => 'sendEmail',
        );
    }

    /**
     * @param Event $event
     */
    public function sendEmail($event)
    {
        $message = Swift_Message::newInstance();

        $message
            ->setSubject(!empty($event->getParam()['subject']) ? 'nika.com -' . $event->getParam()['subject'] : 'nika.com')
            ->setFrom($this->container->getParameter('mail_sender'));

        if ($event->getParam()['template'] == 'holidays') {
            $message->setTo($event->getParam()['sendTo']);
        } else {
            $message->setTo(!empty($event->getParam()['sendTo']) ? $event->getParam()['sendTo'] : $this->container->getParameter('mail_feedback'), 'nika.com');
        }
        $message->setBody(
            $this->templating->render(
                'WebsiteFrontendBundle:Mail:' . $event->getParam()['template'] . '.html.twig', $event->getParam()['param']
            ), 'text/html'
        );

        $this->_mailer->send($message);
    }


}
