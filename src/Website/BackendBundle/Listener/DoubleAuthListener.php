<?php

namespace Website\BackendBundle\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Website\BackendBundle\Events;

class DoubleAuthListener implements EventSubscriberInterface
{
    private $session;

//
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public static function getSubscribedEvents()
    {
        return [
            Events::DOUBLE_AUTH => 'doubleAuth'
        ];
    }

    public function doubleAuth($event)
    {
        $this->session->set('authorization', true);
    }


}