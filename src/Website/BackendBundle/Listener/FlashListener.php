<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Website\BackendBundle\Listener;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\TranslatorInterface;
use Website\BackendBundle\Events;

class FlashListener implements EventSubscriberInterface
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * FlashListener constructor.
     *
     * @param Session $session
     * @param TranslatorInterface $translator
     */
    public function __construct(Session $session, TranslatorInterface $translator)
    {
        $this->session = $session;
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            Events::SESSION_FLASH_SUCCESS => 'addSuccessFlash',
            Events::SESSION_FLASH_ERROR => 'addErrorFlash',
        );
    }

    /**
     * @param Event $event
     */
    public function addSuccessFlash($event)
    {
        $params = array();
        if (array_key_exists('params', $event->getFlash())) {
            $params = $event->getFlash()['params'];
        }
        $this->session->getFlashBag()->add('success', $this->trans($event->getFlash()['message'], $params));
    }

    /**
     * @param string $message
     * @param array $params
     *
     * @return string
     */
    private function trans($message, array $params = array())
    {
        return $this->translator->trans($message, $params, 'messages');
    }

    /**
     * @param Event $event
     */
    public function addErrorFlash($event)
    {
        $params = array();
        if (array_key_exists('params', $event->getFlash())) {
            $params = $event->getFlash()['params'];
        }

        $this->session->getFlashBag()->add('danger', $this->trans($event->getFlash()['message'], $params));

    }

}
