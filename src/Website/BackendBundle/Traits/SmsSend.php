<?php

namespace Website\BackendBundle\Traits;

use BSG;
use Website\BackendBundle\Model\Data;

trait SmsSend
{

    /*SMS подтверждения*/
    public function smsSendCurl($pin, $phone, $BaseDir)
    {
        if (Data::$sendSMS) {
            require_once $BaseDir . "src/Website/BackendBundle/Services/BSG/BSG.php";
            require_once $BaseDir . 'src/Website/BackendBundle/Services/BSG/SmsApiClient.php';
            require_once $BaseDir . 'src/Website/BackendBundle/Services/BSG/ViberApiClient.php';

            $BSG = new BSG('live_iYDt6DXwwoRdyYeA26OR', $phone);

            $smsClient = $BSG->getSmsClient();
            $smsClient->sendSms(
                $phone,
                'website:' . $pin,
                'successSend' . (string)time()
            );

        }
    }

    /*SMS подтверждения*/
    public function smscSendCurl($pin, $phone, $BaseDir)
    {
        /*Отправка через curl */
        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, 'https://smsc.ru/sys/send.php');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, "login=nika.com&psw=Alexey24&phones=%2B" . $phone . "&mes=nika: " . $pin . "&fmt=3");
            $response = json_decode(curl_exec($curl), true);

            curl_close($curl);

        }

        if (!empty($response['error'])) {
            switch ($response['error_code']) {
                case 7:
                    $errorText = "Неверный формат номера телефона.";
                    break;
                case 8:
                    $errorText = "Сообщение на указанный номер не может быть доставлено.";
                    break;
                case 9:
                    $errorText = "Отправка более одного одинакового запроса на передачу SMS-сообщения либо более пяти одинаковых запросов на получение стоимости сообщения в течение минуты.";
                    break;
            }
            //return $errorText;
        }


        return $response;
    }
}