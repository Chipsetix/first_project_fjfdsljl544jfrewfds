<?php

namespace Website\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SEORepository extends EntityRepository
{

    public function findInfo()
    {
        return $this
            ->createQueryBuilder('pi')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();

    }

    public function findSeo($Page)
    {
        $Qb = $this->createQueryBuilder('n');
        $Qb->select('n.metaTitle' . $Page . ' as title,n.metaDesc' . $Page . ' as description,n.metaKeywords' . $Page . ' as keywords');
        $result = $Qb
            ->getQuery()
            ->getOneOrNullResult();;
        return $result;
    }


    public function findSeoLang($lang)
    {

        $Qb = $this->createQueryBuilder('n');

        $Result = $Qb->add(
            'where', $Qb->expr()->andX(
            'n.locale = :locale'
        )
        )
            ->setParameter('locale', $lang)
            ->getQuery()
            ->getResult();
        return $Result;
    }

}