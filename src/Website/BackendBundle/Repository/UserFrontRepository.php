<?php

namespace Website\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Website\BackendBundle\Constant;

class UserFrontRepository extends EntityRepository
{
    public function findUsersBySearch($column, $param)
    {

        $Qb = $this->createQueryBuilder('u');

        $Result = $Qb->add(
            'where', $Qb->expr()->andX(
            'u.' . $column . ' LIKE :key'
        )
        )
            ->orderBy('u.id', 'ASC')
            ->setParameter('key', '%' . $param . '%')
            ->getQuery()
            ->getResult();

        return $Result;
    }

    public function findUsersByIp($users)
    {

        $Qb = $this->createQueryBuilder('u');

        $res = $Qb->andWhere('u.id IN (:ids)')
            ->setParameter('ids', $users)
            ->getQuery()
            ->getResult();

        return $res;
    }

    public function findCountUsers()
    {
        return (int)$this->_em->createQuery('SELECT COUNT(r.id) FROM ' . Constant::TABLE_USER_FRONTEND . ' r')->getSingleScalarResult();

    }

    public function findUsersIdArray()
    {
        $dql = 'select u.id ';
        $dql .= ' FROM WebsiteBackendBundle:UserFrontend u';
        $Result = $this->_em->createQuery($dql)->execute();

        return $Result;
    }

    /*Достает пользователей со смещением*/
    public function findUserOffset($offset, $limit)
    {
        $dql = 'select u';
        $dql .= ' FROM WebsiteBackendBundle:UserFrontend u';
        $dql .= ' order By u.id DESC';
        $Result = $this->_em->createQuery($dql)->setFirstResult($offset)->setMaxResults($limit)->execute();

        return $Result;
    }

    public function findEmails()
    {
        $dql = 'select n.email ';
        $dql .= ' FROM WebsiteBackendBundle:UserFrontend n';
        $dql .= ' where n.enabled=' . true . '';

        return $this->_em->createQuery($dql)->getArrayResult();
    }
}
