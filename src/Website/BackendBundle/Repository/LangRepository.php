<?php

namespace Website\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;

class LangRepository extends EntityRepository
{
    public function findLangByDisableSort()
    {
        $dql = 'select n';
        $dql .= ' FROM WebsiteBackendBundle:Lang n';
        $dql .= ' where  n.disable =false';
        $dql .= ' order By n.con_num ASC';
        $Result = $this->_em->createQuery($dql)->execute();

        return $Result;
    }


}
