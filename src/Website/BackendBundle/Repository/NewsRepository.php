<?php

namespace Website\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;

class NewsRepository extends EntityRepository
{
    public function findNewsByLocale($locale)
    {
        $dql = 'select n';
        $dql .= ' FROM WebsiteBackendBundle:News n';
        $dql .= ' where n.locale = \'' . $locale . '\'';
//        $dql .= ' and  n.Photo IS NOT NULL ';
        $dql .= ' order By n.id DESC';
        $Result = $this->_em->createQuery($dql)->execute();

        return $Result;
    }

    public function findNewsRecords($locale, $slug, $count)
    {
        $Qb = $this->createQueryBuilder('n');

        $Result = $Qb->add(
            'where', $Qb->expr()->andX(
            'n.locale = :locale',
            'n.slug != :slug'
        )
        )
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->setMaxResults($count)
            ->getQuery()
            ->getResult();
        return $Result;

    }


    public function findFourLastNewsByLocale($locale)
    {
        $Qb = $this->createQueryBuilder('n');

        $Result = $Qb->add(
            'where', $Qb->expr()->andX(
            'n.locale = :locale'
        )
        )
            ->orderBy('n.id', 'DESC')
            ->setParameter('locale', $locale)
            ->setMaxResults(4)
            ->getQuery()
            ->getResult();
        return $Result;

    }
}
