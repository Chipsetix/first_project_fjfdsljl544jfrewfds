<?php

namespace Website\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;

class DopageRepository extends EntityRepository
{

    public function findDopage($lang)
    {
        $Qb = $this->createQueryBuilder('n');

        $Result = $Qb->add(
            'where', $Qb->expr()->andX(
            'n.locale = :locale'
        )
        )
            ->setParameter('locale', $lang)
            ->getQuery()
            ->getResult();
        return $Result;

    }
}
