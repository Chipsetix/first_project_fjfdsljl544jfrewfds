<?php

namespace Website\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;

class FaqRepository extends EntityRepository
{
    public function findFaq($lang)
    {

        $dql = 'select  t';
        $dql .= ' FROM WebsiteBackendBundle:FAQ t';
        $dql .= ' inner JOIN t.lang d';
        $dql .= ' where d.locale = \'' . $lang . '\'';
        $dql .= ' order By t.id DESC';

        $Result = $this->_em->createQuery($dql)->execute();

        return $Result;
    }


}