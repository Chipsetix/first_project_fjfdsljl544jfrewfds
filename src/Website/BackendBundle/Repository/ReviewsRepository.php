<?php

namespace Website\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ReviewsRepository extends EntityRepository
{
    public function findAllByStatus()
    {
        $Qb = $this->createQueryBuilder('n');

        $Result = $Qb->add(
            'where', $Qb->expr()->andX(
            'n.status = :status'
        )
        )
            ->setParameter('status', true)
            ->getQuery()
            ->getResult();
        return $Result;

    }

    public function findByTypeAndStatus($type)
    {
        $Qb = $this->createQueryBuilder('n');

        $Result = $Qb->add(
            'where', $Qb->expr()->andX(
            'n.type = :type',
            'n.status = :status'
        )
        )
            ->setParameter('type', $type)
            ->setParameter('status', true)
            ->orderBy('n.id', 'DESC')
            ->getQuery()
            ->getResult();
        return $Result;

    }


}
