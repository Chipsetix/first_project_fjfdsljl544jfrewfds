<?php

namespace Website\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ConfigSiteRepository extends EntityRepository
{

    public function findInfo()
    {
        return $this
            ->createQueryBuilder('pi')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();

    }

    public function findCountriesAndUsers()
    {
        $dql = ' SELECT r.countries, r.users ';
        $dql .= ' FROM  WebsiteBackendBundle:ConfigSite r ';

        $Result = $this->_em->createQuery($dql)->execute();

        return $Result;

    }

    public function countAllUsersById()
    {
        $dql = ' SELECT count(r.id)';
        $dql .= ' FROM  WebsiteBackendBundle:UserFrontend r ';

        $Result = $this->_em->createQuery($dql)->execute();

        return $Result;

    }

}