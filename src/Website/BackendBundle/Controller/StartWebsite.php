<?php

namespace Website\BackendBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Website\BackendBundle\Entity\ConfigSite;
use Website\BackendBundle\Entity\Lang;

class StartWebsite
{

    private $container;

    private $kernel;

    public function __construct(ContainerInterface $container, $kernel)
    {
        $this->container = $container;
        $this->kernel = $kernel;
    }

    public function create($domen, $lang, $i = 2)
    {
        $em = $this->container->get('doctrine')->getManager();

        $configSite = new ConfigSite;

        $configSite->setBaseDir($this->kernel->getProjectDir() . DIRECTORY_SEPARATOR);

        if ('dev' === $this->kernel->getEnvironment()) {
            $domen .= '/app_dev.php';
            $i = 1;
        }

        $configSite->setDomain($domen);

        $Lang = new Lang();

        $Lang->setConNum(0);
        $Lang->setDisable(0);
        $Lang->setTitle($lang == 'ru' ? 'Русский' : 'Английский');
        $Lang->setLocale($lang);
        $Lang->setSlug($lang == 'ru' ? 'Russia' : 'United-Kingdom');

        $em->persist($Lang);
        $em->persist($configSite);

        $em->flush();
    }

}