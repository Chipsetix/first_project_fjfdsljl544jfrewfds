<?php

namespace Website\BackendBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Website\BackendBundle\Constant;

class Info
{

    private $container;

    private $_Helper_class;


    public function __construct(ContainerInterface $container, $HelperClass)
    {
        $this->container = $container;

        $this->_Helper_class = $HelperClass;

    }

    public function sendEmail($holiday)
    {
        $title = $holiday->getTitle();
        $description = $holiday->getDescription();

        $em = $this->container->get('doctrine')->getManager();

        $emails = $em->getRepository(Constant::TABLE_USER_FRONTEND)->findEmails();

        if ($emails) {
            foreach ($emails as $res) {

                $params = [
                    'sendTo' => $res['email'],
                    'template' => 'holidays',
                    'subject' => $title,
                    'param' => [
                        'title' => $title,
                        'description' => $description,
                    ]
                ];

                $this->getHelperClass()->dispatchSendEmail($params);
            }

        }

        $holiday->setEmailComplete(true);
        $em->persist($holiday);
        $em->flush();
    }

    public function getHelperClass()
    {
        return $this->_Helper_class;
    }


}