<?php

namespace Website\UserBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Website\BackendBundle\Entity\UserFrontend as Entity;
use Website\BackendBundle\Model\Data;
use Website\BackendBundle\Services\Composite;
use Website\UserBundle\Form\Admin\UserAdminType as Form;

Class UserAdminDecorator
{
    /**
     * Model class
     *
     * @var Model
     */
    private $_Composite;

    /**
     * Table name
     *
     * @var string
     */
    private $_table_name;

    /**
     * @var Object ContainerInterface
     */
    private $_helper;

    /**
     * @param Composite $Composite
     * @param string $tableName
     * @param int $limitPerPage
     */
    public function __construct(Composite $Composite, $tableName, $ChildrenClass, $HelperClass, $limitPerPage)
    {
        $this->_Composite = $Composite;

        $this->_table_name = $tableName;

        $this->_children_class = $ChildrenClass;

        $this->_helper = $HelperClass;
        #init repos
        $this
            ->setLimitPerPage($limitPerPage)
            ->getComposite()
            ->getEntityManagerSugar()
            ->setTableName($this->getTableName());

    }

    /**
     * Get Model
     *
     * @return Intra\AdminBundle\Services\Model
     */
    public function getComposite()
    {
        return $this->_Composite;
    }

    public function setLimitPerPage($limit)
    {
        $this
            ->getComposite()
            ->getEntityManagerSugar()
            ->setLimitPerPage($limit);

        return $this;
    }

    /**
     * Returns table name
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->_table_name;
    }

    /**
     * Returns table name
     *
     * @return string
     */
    public function getChildrenClass()
    {
        return $this->_children_class;
    }

    /**
     * Show All rows
     *
     * @return Array
     */
    public function ShowAll($page)
    {
        $limit = Data::$limit_page_admin;

        $offset = Data::offset($page, $limit);

        /*Достаем 100 пользователей со смещением чтобы не доставать всех*/
        $Response['entities'] = $this->getRepository('UserFrontend')->findUserOffset($offset, $limit);

        $Response['knp'] = $this->getHelperClass()
            ->paginator(
                $this->getRepository('UserFrontend')->findUsersIdArray(),
                $page,
                $limit
            );

        $Response['count'] = $this->getRepository('UserFrontend')->findCountUsers();

        $Response['offset'] = $offset;

        return $Response;
    }

    private function getRepository($tableName)
    {
        return $this->getComposite()
            ->getEntityManagerSugar()
            ->getRepository('WebsiteBackendBundle:' . $tableName . '');
    }

    public function getHelperClass()
    {
        return $this->_helper;
    }

    public function Search($request, $page)
    {
        $request = $request->query->get('search_filtr_form');
        $Response = $this->getRepository('UserFrontend')->findUsersBySearch($request['column'], $request['input']);


        $Response['entities'] = $this->getHelperClass()->paginator($Response, $page);
        $Response['column'] = $request['column'];
        $Response['input'] = $request['input'];
        $Response['count'] = count($Response['entities']);

        return $Response;

    }

    /**
     * Returns array for render view "Create new element"
     *
     * @return Array
     */
    public function Novel()
    {
        return $this->getComposite()->Novel(new Entity(), Form::class);

    }

    /**
     * Show one row by id
     *
     * @param int $id
     * @param int $tab
     * @return Array
     */
    public function Show($id, $tab = null)
    {

        $user = $this->getRepository('UserFrontend')->findOneBy(['id' => $id]);

        $Response = $this->getComposite()->Show($id);

        $Response['tab'] = $tab;

        return $Response;
    }

    /**
     * Returns array for render view "Edit element"
     *
     * @param int $id
     *
     * @return Array
     */
    public function Edit($id)
    {
        return $this->getComposite()->Edit($id, Form::class);
    }

    /**
     * Returns array for render view "Edit element"
     *
     * @param int $id
     * @param object $FormType
     *
     * @return Array
     */
    public function EditDoper($id, $FormType)
    {
        return $this->getComposite()->Edit($id, $FormType);
    }

    /**
     * Update element and redirect
     *
     * @param Request $Request
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function Update(Request $Request, $id)
    {
        return $this
            ->getComposite()
            ->Update(
                $Request,
                $id,
                Form::class,
                'website_admin_user_show',
                array('id' => $id)
            );
    }
}