<?php

namespace Website\UserBundle\Services;

use DateTime;
use Doctrine\ORM\EntityManager;
use Website\BackendBundle\Services\Composite;
use Website\UserBundle\Form\ChangePasswordFormType as FormPassword;
use Website\UserBundle\Form\UserEditType as Form;

# ActionInterface
Class ProfileDecorator
{
    /**
     * Model class
     *
     * @var Model
     */
    private $_Composite;

    private $_table_name;
    /**
     * Entity manager
     * @var Object insans of EntityManager
     */
    private $_EntityManager;
    /**
     * Container
     * @var Object $Container
     */
    private $_Container;

    /**
     * @var Object ContainerInterface
     */
    private $_Helper_class;

    /**
     * @param Composite $Composite
     * @param $_Helper_class $HelperClass
     */
    public function __construct(Composite $Composite, $tableName, EntityManager $em, $Container, $HelperClass)
    {
        $this->_Composite = $Composite;

        $this->_table_name = $tableName;

        $this->_EntityManager = $em;

        $this->_Container = $Container;

        $this->_Helper_class = $HelperClass;

        $this
            ->getComposite()
            ->getEntityManagerSugar()
            ->setTableName($this->getTableName());
    }

    /**
     * Get Model
     *
     * @return Intra\AdminBundle\Services\Model
     */
    public function getComposite()
    {
        return $this->_Composite;
    }

    /**
     * Returns table name
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->_table_name;
    }

    /**
     * Returns instans of EntityManager
     *
     * @return Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->_EntityManager;
    }

    public function getContainer()
    {
        return $this->_Container;
    }

    public function getHelperClass()
    {
        return $this->_Helper_class;
    }

    public function Edit($request, $user, $slug)
    {
        $FormEdit = $this->getComposite()
            ->getFormSugar()
            ->createEditForm(
                Form::class,
                $this
                    ->getRepository('UserFrontend')->findOneById($user->getId())
            );

        $Response = $this->getComposite()->Edit($user->getId(), Form::class);
        $Response['edit_form'] = $FormEdit->createView();

        $FormPassword = $this->getComposite()
            ->getFormSugar()
            ->createEditForm(
                FormPassword::class,
                $this
                    ->getRepository('User')->findOneById($user->getId())
            );
        $Response['password_form'] = $FormPassword->createView();

        $Response['user'] = $user;
        $Response['page'] = 'settings';
        $Response['slug'] = $slug;

        return $Response;
    }

    private function getRepository($tableName)
    {
        return $this->getComposite()
            ->getEntityManagerSugar()
            ->getRepository('WebsiteBackendBundle:' . $tableName . '');
    }

    public function getHeader($user)
    {
        $Response['servertime'] = new DateTime(date('H:i'));
        $Response['user'] = $user;

        return $Response;
    }

    public function getHeaderLang($user, $route, $route_params)
    {
        $Response['servertime'] = new DateTime(date('H:i'));
        $Response['user'] = $user;
        $Response['route'] = $route;
        $Response['route_params'] = $route_params;

        return $Response;
    }


    public function sortHistory($history)
    {
        if (is_array($history)) {

            foreach ($history as $key => $res) {
                foreach ($res->getDateCreate() as $key2 => $row) {

                    $dateArray[$key] = strtotime($row);
                    break;
                }
            }
            array_multisort($dateArray, SORT_NUMERIC, SORT_ASC, $history);

            return $history;

        }
        return array();
    }

}