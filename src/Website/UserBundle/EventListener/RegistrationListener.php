<?php

namespace Website\UserBundle\EventListener;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RegistrationListener implements EventSubscriberInterface
{
    /**
     * @var Object EntityManager
     */
    private $_EntityManager;
    /**
     * @var Object ContainerInterface
     */
    private $_Container;
    /**
     * @var Object ContainerInterface
     */
    private $_Helper_class;
    /**
     * @var Object ContainerInterface
     */
    private $_Children_class;

    public function __construct(EntityManager $em, ContainerInterface $Container, $HelperClass, $ChildrenClass)
    {
        $this->_EntityManager = $em;
        $this->_Container = $Container;
        $this->_Helper_class = $HelperClass;
        $this->_Children_class = $ChildrenClass;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_INITIALIZE => 'onRegistrationInitialize',
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
            FOSUserEvents::REGISTRATION_COMPLETED => 'onRegistrationCompleted',
        );
    }

    public function getChildrenClass()
    {
        return $this->_Children_class;
    }

    public function onRegistrationInitialize()
    {
    }

    public function onRegistrationSuccess(FormEvent $event)
    {
        $em = $this->getEntityManager();

        $request = $this
            ->getContainer()
            ->get('request_stack')
            ->getCurrentRequest();

        /** @var $user UserInterface */
        $user = $event->getForm()->getData();

        if (!preg_match("|^[-a-z0-9_]+$|i", $user->getUsername())) exit("Login contains invalid characters");
    }

    public function getEntityManager()
    {
        return $this->_EntityManager;
    }

    public function getContainer()
    {
        return $this->_Container;
    }

    public function onRegistrationCompleted()
    {

        $Request = $this
            ->getContainer()
            ->get('request_stack')
            ->getCurrentRequest();

        $Post = $Request->request->get('fos_user_registration_form');

        $params = [
            'sendTo' => $Post['email'],
            'template' => 'registration',
            'subject' => 'You signed up',
            'param' => [
                'username' => $Post['username'],
                'locale' => $Request->getLocale(),
//                'secret'   => $Post['secretWord'],
                'email' => $Post['email'],
            ]
        ];

        $this->getHelperClass()->dispatchSendEmail($params);

    }

    public function getHelperClass()
    {
        return $this->_Helper_class;
    }

}