<?php

namespace Website\UserBundle\Event;

use FOS\UserBundle\Model\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserListener implements EventSubscriberInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorageInterface;

    /**
     * @var RouterInterface
     */
    private $routerInterface;
    /**
     * @var Object ContainerInterface
     */
    private $_Helper_class;

    /**
     * Container
     * @var Object $Container
     */
    private $_Container;

    public function __construct(TokenStorageInterface $tokenStorageInterface, RouterInterface $routerInterface, $HelperClass, $Container)
    {
        $this->tokenStorageInterface = $tokenStorageInterface;
        $this->routerInterface = $routerInterface;
        $this->_Helper_class = $HelperClass;
        $this->_Container = $Container;
    }

    public static function getSubscribedEvents()
    {
        return array(
//            FOSUserEvents::REGISTRATION_INITIALIZE => 'onRegistrationInitialize',
//            FOSUserEvents::REGISTRATION_SUCCESS    => 'onRegistrationSuccess',
//            FOSUserEvents::REGISTRATION_COMPLETED  => 'onRegistrationCompleted',
        );
    }

    public function getHelperClass()
    {
        return $this->_Helper_class;
    }

    public function getContainer()
    {
        return $this->_Container;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (in_array($_SERVER['REMOTE_ADDR'], ['95.216.197.227'])) return;
//        $request = $event->getRequest();

        if ($event->getRequest()->attributes->get('_route') == 'fos_user_security_login') {
            $this->securityLoginUser($event);
        }

        if (
            preg_match('/profile/', $_SERVER['REQUEST_URI']) ||
            (preg_match('/login/', $_SERVER['REQUEST_URI']) && empty($_SERVER['HTTP_REFERER']))
        ) {
            if ($this->_Container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY') == false) {
                return $event->setResponse(
                    new RedirectResponse(
                        $this->routerInterface->generate('website_frontend_homepage')
                    )
                );
            }
        }

        if (
            $event->getRequest()->attributes->get('_route') == 'fos_user_registration_register' ||
            $event->getRequest()->attributes->get('_route') == 'website_frontend_homepage'
        ) {
//            if (!empty($_GET["ref"])) {
//
//                $UplinerName = $this->getHelperClass()->getUserName($_GET["ref"]);
//
//                $event->getRequest()->cookies->set("websitecookie", $UplinerName->getUsername(), time() + 3600 * 24 * 3, "/", 'nika.com', 1);
//            }
        }

    }

    public function securityLoginUser($event)
    {
        if ($this->tokenStorageInterface->getToken() === null) {
            return false;
        }

        if ($this->tokenStorageInterface->getToken() instanceof AnonymousToken) {
            return false;
        }

        if (!$this->tokenStorageInterface->getToken()->getUser() instanceof User) {
            return false;
        }

        return $event->setResponse(
            new RedirectResponse(
                $this->routerInterface->generate('fos_user_profile_show')
            )
        );
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (
            $event->getRequest()->attributes->get('_route') == 'fos_user_registration_register' ||
            $event->getRequest()->attributes->get('_route') == 'website_frontend_homepage'
        ) {
            $response = $event->getResponse();
            $request = $event->getRequest();

//            if (!empty($_GET["ref"])){
//
//                $response->headers->setCookie(new Cookie('websitecookie',$request->cookies->get('websitecookie')));
//            }
            $request->getSession()->getFlashBag()->clear();
        }

    }
}