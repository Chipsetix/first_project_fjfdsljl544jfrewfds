<?php

namespace Website\UserBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Http\HttpUtils;

class AuthenticationSuccessHandler extends DefaultAuthenticationSuccessHandler
{

    protected $router;
    protected $httpUtils;
    private $em;
    private $container;

    public function __construct(HttpUtils $httpUtils, Router $router, ObjectManager $em, $container)
    {

        $this->httpUtils = $httpUtils;
        $this->router = $router;
        $this->em = $em;
        $this->container = $container;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {

        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {

            return $this->httpUtils->createRedirectResponse($request, 'website_admin_homepage');
        }


        if ($token->getUser()->getTries() > 0) {

            $token->getUser()->setTries(0);
            $this->em->persist($token->getUser());

        }
        $this->em->flush();

        return $this->httpUtils->createRedirectResponse($request, $this->determineTargetUrl($request));
    }

}