<?php

namespace Website\UserBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationFailureHandler;
use Symfony\Component\Security\Http\HttpUtils;
use Website\BackendBundle\Model\Data;

class AuthenticationFailureHandler extends DefaultAuthenticationFailureHandler
{
//class AuthenticationFailureHandler implements AuthenticationFailureHandlerInterface {

    protected $router;
    protected $httpUtils;
    private $em;
    private $container;

    public function __construct(HttpUtils $httpUtils, Router $router, ObjectManager $em, $container)
    {

        $this->httpUtils = $httpUtils;
        $this->router = $router;
        $this->em = $em;
        $this->container = $container;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if ($request->request->get('_username')) {

            $User = $this->em->getRepository('WebsiteBackendBundle:User')->findOneByUsername($request->request->get('_username'));

            /**
             * Есл ипользователя нет в БД переадресовать на регистрацию
             */
            if (!$User) {
//                $url=$this->container->get('router')->generate('fos_user_registration_register');
//                return new RedirectResponse($url);
            } else {

                if ($User->isEnabled() == true) {
                    $User->setTries($User->getTries() + 1);

                    if ($User->getTries() >= Data::$tries && $User->getUsername() !== 'admin') {
                        //$User->setEnabled(false);
                        $User->setTries(0);
                    }

                    $this->em->persist($User);
                    $this->em->flush();
                }
            }
        }

        // $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);
        //return new RedirectResponse($this->container->get('router')->generate('fos_user_security_login'));

        return parent::onAuthenticationFailure($request, $exception);
        //return $this->redirectToRoute('fos_user_security_login');
    }

}