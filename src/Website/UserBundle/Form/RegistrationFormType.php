<?php

namespace Website\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, array(
                'attr' => array('class' => 'field', 'autocomplete' => "off", 'data-theme' => 'advanced', 'maxlength' => '18', 'minlength' => '4'),
                'trim' => true,
                //'translation_domain' => 'FOSUserBundle'
            ))
            ->add('email', null, array(
                'attr' => array('class' => 'field', 'autocomplete' => "off", 'data-theme' => 'advanced'),
                'trim' => true,
                //  'translation_domain' => 'FOSUserBundle'
            ))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'options' => array(
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => array(
                        'autocomplete' => 'new-password',
                    ),
                ),
                'first_options' => array('attr' => array('class' => 'field ')),
                'second_options' => array('attr' => array('class' => 'field ')),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
            ->add('send', SubmitType::class, array(
                    'label' => 'registration.btn_reg',
                    'attr' => array('class' => 'btn btn_alt', 'data-theme' => 'advanced')
                )
            );
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    // For Symfony 2.x

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}