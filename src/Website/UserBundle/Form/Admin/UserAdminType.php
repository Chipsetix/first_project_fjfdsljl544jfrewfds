<?php

namespace Website\UserBundle\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserAdminType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, array(
                'label' => 'form.email',
                'label_attr' => array('class' => 'col-sm-2 control-label'),
                'attr' => array('class' => 'form-control', 'autocomplete' => 'off', 'data-theme' => 'advanced', 'placeholder' => 'e-mail'),
                'trim' => true,
                // 'translation_domain' => 'FOSUserBundle'
            ))
            ->add('username', null, array(
                'label' => 'form.username',
                'label_attr' => array('class' => 'col-sm-2 control-label'),
                'attr' => array('class' => 'form-control', 'autocomplete' => 'off', 'data-theme' => 'advanced', 'placeholder' => 'Логин'),
                'trim' => true,
                //'translation_domain' => 'FOSUserBundle'
            ))
//                ->add('secretWord', NumberType::class, array(
//                        'label' => 'PIN code',
//                        'label_attr' => array('class' => 'col-sm-2 control-label'),
//                        'attr' => array('class' => 'form-control','autocomplete'=>'off', 'data-theme' => 'advanced', 'placeholder' => '111111'),
//                        'required' => false
//                    )
//                )

//                ->add('advertising', CheckboxType::class, array(
//                        'label' => 'Согласие на рекламу',
//                        'label_attr' => array('class' => 'col-sm-2 control-label'),
//                        'attr' => array('class' => 'minimal form-control'),
//                        'required' => false
//                    )
//                )
            ->add('enabled', CheckboxType::class, array(
                    'label' => 'Включен',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'minimal form-control'),
                    'required' => false
                )
            )
//                ->add('sms', null, array(
//                        'label' => 'Включить авторизацию sms',
//                        'label_attr' => array('class' => 'col-sm-2 control-label'),
//                        'attr' => array('class' => 'minimal form-control'),
//                        'required' => false
//                    )
//                )
            ->add('dateCreate', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => true,
                'label' => 'Дата создания',
                'label_attr' => array('class' => 'col-sm-2 control-label'),
                'attr' => array('class' => 'form-control pull-righ datepicker', 'data-theme' => 'advanced', 'placeholder' => 'dd-mm-yyyy')
            ))
            ->add('comment', TextareaType::class, array(
                    'label' => 'Примечание',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => 'Примечание о пользователе, виден только  админу'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => false
                )
            )
            ->add('send', SubmitType::class, array(
                    'label' => 'Отправить',
                    'attr' => array('class' => 'form-control col-sm-2 btn btn-success', 'data-theme' => 'advanced')
                )
            );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(

            'translation_domain' => 'FOSUserBundle'

        ));
    }
//    public function getParent()
//    {
//        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
//    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    // For Symfony 2.x

    public function getBlockPrefix()
    {
        return 'website_user_edit';
    }

}