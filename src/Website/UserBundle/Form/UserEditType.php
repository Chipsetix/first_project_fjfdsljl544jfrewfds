<?php

namespace Website\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('photo', FileType::class, array(
                    'label' => null,
                    'attr' => array('class' => 'form-control', 'autocomplete' => 'off', 'data-theme' => 'advanced', 'placeholder' => 'registry.photo'),
                    'trim' => true,
                    'required' => false
                )
            );
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(

            'translation_domain' => 'messages',

        ));
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    // For Symfony 2.x

    public function getBlockPrefix()
    {
        return 'website_user_edit';
    }
}