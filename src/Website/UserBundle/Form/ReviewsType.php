<?php

namespace Website\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReviewsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('videoUrl', TextType::class,
                array(
                    'label' => 'front.reviews.link',
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => 'front.reviews.link'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => true
                )
            )
            ->add('type', HiddenType::class, array(
                'data' => 'ytb',
            ));
    }


    /**
     * @param configureOptions $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Website\BackendBundle\Entity\Reviews',
            'translation_domain' => 'messages',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'userbundle_reviews';
    }
}