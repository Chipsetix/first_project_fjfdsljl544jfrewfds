<?php

namespace Website\UserBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Website\BackendBundle\Constant;

/**
 * Controller managing the user profile
 *
 * @author Christophe Coevoet <stof@notk.org>
 */
class ProfileController extends Controller
{

    public function showAction(Request $request)
    {

        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {

            $user = $this->getUser();

            if ($user->getDoubleAuth() == true && empty($_SESSION['authorization'])) {
                return $this->redirectToRoute('website_frontend_doubleauth_authorization', array('_locale' => $request->getLocale()));
            }

            if (!is_object($user) || !$user instanceof UserInterface) {
                throw new AccessDeniedException('This user does not have access to this section.');
            }


            /* news */
            $news = $this->getDoctrine()->getManager()->getRepository('WebsiteBackendBundle:News')->findFourLastNewsByLocale($request->getLocale());

            $data = [
                'news' => $news
            ];

            return $this->render('FOSUserBundle:Profile:show.html.twig', [
                'user' => $user,
                'data' => $data,
                'req' => $request,

            ]);
        }
        return $this->redirectToRoute('fos_user_security_login');
    }

    /**
     * @Template("WebsiteUserBundle:FrontendProfile:cabinet-edit-show.html.twig")
     */
    public function editShowAction(Request $request, $slug)
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->get('website_user_profile')->Edit($request, $this->getUser(), $slug);

        }
        return $this->redirectToRoute('fos_user_security_login');
    }

    /**
     * @Template("WebsiteUserBundle:FrontendProfile:cabinet-edit.html.twig")
     */
    public function editAction(Request $request, $slug)
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->get('website_user_profile')->Edit($request, $this->getUser(), $slug);

        }
        return $this->redirectToRoute('fos_user_security_login');
    }

    /**
     * @Template("WebsiteUserBundle:FrontendProfile:cabinet-edit.html.twig")
     */
    public function updateAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->get('website_user_profile')->Update($request, $this->getUser(), $request->query->get('slug'));

        }
        return $this->redirectToRoute('fos_user_security_login');

    }


    public function newsLetterAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $Entities = $this->get('website_user_profile')->getNewsLetter($request, $this->getUser());

            return $this->render('WebsiteUserBundle:FrontendProfile:cabinet-newsletter.html.twig', [
                'user' => $this->getUser(),
                'entities' => $Entities,
                'type' => $request->get('type'),
            ]);

        }
        return $this->redirectToRoute('fos_user_security_login');
    }

    /**
     * @Template("WebsiteUserBundle:FrontendProfile:header.html.twig")
     */
    public function getHeaderAction($route, $route_params)
    {
        return $this->get('website_user_profile')->getHeaderLang($this->getUser(), $route, $route_params);
    }


    public function getMenuAction()
    {
        $em = $this->getDoctrine()->getManager();

        $notice = $em->getRepository('WebsiteBackendBundle:Newsletter')->findNewsletter(true);

        $personal = $em->getRepository('WebsiteBackendBundle:Newsletter')->findNewsletterNoReading($this->getUser()->getId(), true, 'false');

        return $this->render('WebsiteUserBundle:FrontendProfile:cabinet-content_left.html.twig', [
            'countNotice' => $notice,
            'countPersonal' => $personal
        ]);
    }

    public function CDNAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $type = $request->request->get('type');


        if ($type == 'news') {
            $Entity = $em->getRepository('WebsiteBackendBundle:News')->findOneById($request->request->get('news'));

        } else {

            $Entity = $em->getRepository('WebsiteBackendBundle:UserFrontend')->findOneByUsername($request->request->get('username'));
        }

        $Entity->setPhoto($request->request->get('photo'));

        $em->persist($Entity);
        $em->flush();

        return $this->redirectToRoute('website_profile_edit', array('_locale' => $request->request->get('locale')));
    }

    public function securitySendAction()
    {
        $user = $this->getUser();
        if ($user->getConfirmationPin() == NULL) {

            $pin = rand(1000, 9999);
            $user->setConfirmationPin($pin);
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

        } else {
            $pin = $user->getConfirmationPin();
        }

        $params = [
            'sendTo' => $user->getEmail(),
            'template' => 'doubleAuth',
            'subject' => 'Mail confirmation',
            'param' => [
                'username' => $user->getUsername(),
                'name' => $user->getFirstName(),
                'surname' => $user->getLAstName(),
                'pin' => $pin,
                'locale' => 'en'
            ]
        ];

        $this->get('website_backend.helper.class')->dispatchSendEmail($params);

        $responce = new JsonResponse();

        $responce->setData(array(
            'type' => 'on'
        ));

        return $responce;
    }

    public function securityOffAction()
    {
        $user = $this->getUser();

        $user->setConfirmationPin(null);
        $user->setDoubleAuth(false);
        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();

        $responce = new JsonResponse();

        $responce->setData(array(
            'url' => $this->generateUrl('fos_user_change_password'),
            'type' => 'off'
        ));

        return $responce;
    }

    public function securityPinAction(Request $request)
    {
        $responce = new JsonResponse();
        $user = $this->getUser();

        if ($request->request->get('pin') == $user->getConfirmationPin()) {
            $url = $this->generateUrl('fos_user_change_password');

            $user->setConfirmationPin(null);
            $user->setDoubleAuth(true);
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $_SESSION['authorization'] = true;

        } else {

            $url = null;

            $alertTwig = $this->render('WebsiteUserBundle:FrontendProfile:alert.html.twig',
                array(
                    'message' => $this->get('translator')->trans(Constant::DANGER_PIN)
                )
            );
            $twig = $alertTwig->getContent();

        }

        $responce->setData([
                'url' => $url,
                'alertTwig' => @$twig
            ]
        );

        return $responce;
    }

    public function getLangAction(Request $req, $route, $route_params)
    {
        $em = $this->getDoctrine()->getManager();

        $Entities = $em->getRepository('WebsiteBackendBundle:Lang')->findLangByDisableSort();

        return $this->render('WebsiteUserBundle:FrontendProfile:lang.html.twig',
            array(
                'req' => $req,
                'langs' => $Entities,
                'curr_lang' => $req->getLocale(),
                'route' => $route,
                'route_params' => $route_params,
            )
        );
    }

    public function paginator($result, $page)
    {
        $paginator = $this->get('knp_paginator');

        return $paginator->paginate($result, $page, 10);
    }

}
