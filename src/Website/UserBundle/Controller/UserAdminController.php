<?php

namespace Website\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserAdminController extends Controller
{
    /**
     * @Template("WebsiteUserBundle:UserAdmin:index.html.twig")
     */
    public function indexAction($page)
    {
        return $this->get('website_user_admin.services')->ShowAll($page);
//        $Response = $this->getDoctrine()->getManager()->getRepository('WebsiteBackendBundle:UserFrontend')->findAll();

    }

    /**
     * @Template("WebsiteUserBundle:UserAdmin:index.html.twig")
     */
    public function searchAction(Request $request, $page)
    {
        return $this->get('website_user_admin.services')->Search($request, $page);

    }

    /**
     * @Template("WebsiteUserBundle:UserAdmin:form_user_edit.html.twig")
     */
    public function editAction($id)
    {
        return $this->get('website_user_admin.services')->Edit($id);
    }

    public function updateAction(Request $request, $id)
    {
        return $this->get('website_user_admin.services')->Update($request, $id);

    }


    /**
     * @Template("WebsiteUserBundle:UserAdmin:show.html.twig")
     */
    public function showAction(Request $Request, $id)
    {
        return $this->get('website_user_admin.services')->Show($id, $Request->query->get('tab'));
    }


//--------------------------------------------Comment2------------------------------------------------------------------


    public function commentAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        $user = $this->getDoctrine()
            ->getRepository('WebsiteBackendBundle:UserFrontend')
            ->findOneById($id);

        $user->setComment2($request->request->get('comment2'));

        $em->persist($user);
        $em->flush();

        return $this->redirect($this->generateUrl('website_admin_user_show', ['id' => $id]));

    }
}
