<?php

namespace Website\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Website\BackendBundle\Constant;
use Website\BackendBundle\Model\Data;
use Website\FrontendBundle\Form\DoubleAuthType;
use Website\FrontendBundle\Form\Model\DoubleAuth;

class SecurityController extends Controller
{
    public function doubleAuthAction(Request $request)
    {
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        if ($user->getConfirmationPin() == null) {
            /*Создаем рандом код 4 цифры*/
            $code = rand(1000, 9999);

            $this->SendMail($code, $user->getEmail());
        } else {
            $code = $user->getConfirmationPin();
        }
        $user->setConfirmationPin($code);

        $em->persist($user);
        $em->flush();

        $form = $this->createForm(DoubleAuthType::class, new DoubleAuth, array('method' => 'POST'));

        $form->handleRequest($request);
        $mess = null;

        if ($form->isValid()) {

            /*Проверка совпадает ли код */
            if ($request->request->get('double_auth')['code'] == $user->getConfirmationPin()) {

                $_SESSION['authorization'] = true;

                $user->setTries(0);
                $user->setConfirmationPin(NULL);

                $em->persist($user);
                $em->flush();

                return $this->redirectToRoute('website_profile', array('_locale' => $request->getLocale()));
            } else {
                /*Увеличиваем колличество попыток ввода*/
                $user->setTries($user->getTries() + 1);

                $em->persist($user);
                $em->flush();

                if ($user->getTries() >= Data::$tries) {

                    $user->setEnabled(false);
                    $em->persist($user);
                    $em->flush();

                    return $this->redirectToRoute('fos_user_security_logout');

                } else {

                    $translator = $this->get('translator')->transChoice(Constant::NO_VALID_CODE, 1,
                        [
                            '%tries%' => Data::$tries - $user->getTries()
                        ]
                    );
                }
            }

        }

        return $this->render('WebsiteFrontendBundle:DoubleAuth:form_authorization.html.twig',
            array(
                'user' => $user,
                'form' => $form->createView(),
                'page' => 'login',
                'error' => @$translator,
                'doparam' => 'black'
            )
        );
    }

    public function SendMail($code, $userEmail)
    {

        $params = [
            'sendTo' => $userEmail,
            'template' => 'authorize_code_email',
            'subject' => 'Pin code',
            'param' => [
                'code' => $code
            ]
        ];

        $this->get('website_backend.helper.class')->dispatchSendEmail($params);

    }


    /*SMS подтверждения*/
    public function smsCurl($secret, $user, $em)
    {
        /*Запись в БД token*/
        /*Отправка через curl */
        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, 'https://smsc.ru/sys/send.php');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, "login=website@nika.com&psw=ZSNKSayATwU4SGuca8Qv3uhZ7eeQ91&=nika&phones=" . $user->getMobile() . "&mes=Secret: " . $secret . "&fmt=3");
            $response = json_decode(curl_exec($curl), true);

            curl_close($curl);

        }

        if (!empty($response['error'])) {
            switch ($response['error_code']) {
                case 7:
                    $errorText = "Неверный формат номера телефона.";
                    break;
                case 8:
                    $errorText = "Сообщение на указанный номер не может быть доставлено.";
                    break;
                case 9:
                    $errorText = "Отправка более одного одинакового запроса на передачу SMS-сообщения либо более пяти одинаковых запросов на получение стоимости сообщения в течение минуты.";
                    break;
            }
            //return $errorText;
        }


        return $response;
    }

}

