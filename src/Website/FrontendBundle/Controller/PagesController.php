<?php

namespace Website\FrontendBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Website\FrontendBundle\Form\FeedbackType;
use Website\FrontendBundle\Form\Model\Feedback;

class PagesController extends Controller
{
    /**
     * @Template("WebsiteFrontendBundle:Pages:main.html.twig")
     */
    public function indexAction(Request $request)
    {
        return $this->get('website_frontend_pages')->main($request);
    }

    /**
     * @Template("WebsiteFrontendBundle:Pages:news.html.twig")
     */
    public function newsAction($_locale, $page)
    {
        return $this->get('website_frontend_pages')->news($_locale, $page);
    }

    /**
     * @Template("WebsiteFrontendBundle:Pages:news_single.html.twig")
     */
    public function newsSingleAction($_locale, $slug)
    {
        return $this->get('website_frontend_pages')->newsSingle($_locale, $slug);
    }

    /**
     * @Template("WebsiteFrontendBundle:Pages:reviews.html.twig")
     */
    public function rewiewsAction($type, $page)
    {
        return $this->get('website_frontend_pages')->rewiews($type, $page);
    }

    public function pagesAction($_locale, $page)
    {
        $em = $this->getDoctrine()->getManager();
        $doparam = 'white';
        $Entities = $em->getRepository('WebsiteBackendBundle:Dopage')->findBy(array('locale' => $_locale, 'url' => $page));

        return $this->render(
            'WebsiteFrontendBundle:Pages:pages.html.twig',
            array(
                'entities' => $Entities,
                'countpage' => count($Entities),
                'page' => $page,
                'doparam' => $doparam
            )
        );
    }

    public function faqAction($_locale)
    {
        $em = $this->getDoctrine()->getManager();

        $Entities = $em->getRepository('WebsiteBackendBundle:FAQ')->findFaq($_locale);

        return $this->render('WebsiteFrontendBundle:Pages:faq.html.twig', [
            'entities' => $Entities,
            'page' => 'faq',
            'doparam' => 'white'
        ]);
    }

    public function aboutAction($_locale)
    {
        $em = $this->getDoctrine()->getManager();

        $Entities = $em->getRepository('WebsiteBackendBundle:FAQ')->findFaq($_locale);

        return $this->render('WebsiteFrontendBundle:Pages:about.html.twig', [
            'entities' => $Entities,
            'page' => 'faq',
            'doparam' => 'white'
        ]);
    }

    public function sendRewiewsAction(Request $request)
    {
        $this->get('website_frontend_pages')->sendRewiews($request, $this->getUser(), 'ytb');

        return $this->redirectToRoute('website_frontend_reviews', ['_locale' => $request->getLocale(), 'type' => 'ytb']);
    }


    public function getFeedbackFormAction()
    {

        $Type = FeedbackType::class;
        $Ent = new Feedback();
        $twig = 'WebsiteFrontendBundle:viewBlock:feedback_form.html.twig';

        $Form = $this->createForm(
            $Type,
            $Ent
        );

        return $this->render($twig, [
            'form' => $Form->createView()
        ]);
    }

    public function feedbackAction(Request $Request)
    {
        $this->get('website_frontend_pages')->Feedback($Request);

        return $this->redirectToRoute('website_frontend_pages', array('_locale' => $Request->getLocale(), 'page' => 'contacts'));
    }

    public function getLangAction($req, $type)
    {
        $em = $this->getDoctrine()->getManager();

        $Entities = $em->getRepository('WebsiteBackendBundle:Lang')->findLangByDisableSort();

        return $this->render('WebsiteFrontendBundle:viewBlock:lang.html.twig', [
            'req' => $req,
            'langs' => $Entities,
            'type' => $type,
            'curr_lang' => $req->getLocale(),
        ]);
    }

    public function getLangRegAction($req, $type)
    {
        $em = $this->getDoctrine()->getManager();

        $Entities = $em->getRepository('WebsiteBackendBundle:Lang')->findLangByDisableSort();

        return $this->render('WebsiteFrontendBundle:viewBlock:lang-reg.html.twig', [
            'req' => $req,
            'langs' => $Entities,
            'type' => $type,
            'curr_lang' => $req->getLocale(),
        ]);
    }

    public function getActiveLangAction($_locale, $pageforLang = null)
    {
        $em = $this->getDoctrine()->getManager();

        $lang = $em->getRepository('WebsiteBackendBundle:Lang')->findOneByLocale($_locale);

        return $this->render('WebsiteFrontendBundle:viewBlock:active_lang.html.twig', [
            'activeLang' => $lang->getSlug(),
            'pageforLang' => $pageforLang
        ]);
    }

    public function getVersionsAction($type, $template)
    {
        if ($type == 'css') {
            if (file_exists('FrontendAssets/' . $template . '/style/plugin.css')) {
                $prefix = filesize('FrontendAssets/' . $template . '/style/plugin.css');
            }
            if (file_exists('FrontendAssets/' . $template . '/style/app.css')) {
                $prefix2 = filesize('FrontendAssets/' . $template . '/style/app.css');
            }
        } else {
            if (file_exists('FrontendAssets/' . $template . '/script/plugin.js')) {
                $prefix = filesize('FrontendAssets/' . $template . '/script/plugin.js');
            }
            if (file_exists('FrontendAssets/' . $template . '/script/app.js')) {
                $prefix2 = filesize('FrontendAssets/' . $template . '/script/app.js');
            }
        }

        $_template = ($template == 'guest' && $type == 'js') ? $template . '_' : '';

        return $this->render('::versions_' . $_template . $type . '.html.twig', [
            'prefix' => @$prefix,
            'prefix2' => @$prefix2,
            'type' => $type,
            'template' => $template
        ]);
    }


}