<?php

namespace Website\FrontendBundle\Services;

use Doctrine\ORM\EntityManager;
use Website\BackendBundle\Constant;
use Website\BackendBundle\Entity\Reviews;
use Website\BackendBundle\Model\Data;
use Website\BackendBundle\Services\Composite;
use Website\FrontendBundle\Form\FeedbackType;
use Website\FrontendBundle\Form\Model\Feedback;
use Website\FrontendBundle\Form\ReviewsType;

# ActionInterface
Class PagesDecorator
{
    /**
     * Model class
     *
     * @var Model
     */
    private $_Composite;
    /**
     * Entity manager
     * @var Object insans of EntityManager
     */
    private $_EntityManager;
    /**
     * Container
     * @var Object $Container
     */
    private $_Container;

    /**
     * @var Object ContainerInterface
     */
    private $_Helper_class;

    /**
     * @param Composite $Composite
     * @param $_Helper_class $HelperClass
     */
    public function __construct(Composite $Composite, EntityManager $em, $Container, $HelperClass)
    {
        $this->_Composite = $Composite;
        $this->_EntityManager = $em;
        $this->_Container = $Container;
        $this->_Helper_class = $HelperClass;
    }

    public function getContainer()
    {
        return $this->_Container;
    }

    public function main($request)
    {
        $Response['page'] = 'main';
        $Response['entities'] = $this
            ->getEntityManager()
            ->getRepository('WebsiteBackendBundle:Dopage')
            ->findBy([
                'locale' => $request->getLocale(),
                'url' => 'main'
            ]);

        return $Response;
    }

    /**
     * Returns instans of EntityManager
     *
     * @return Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->_EntityManager;
    }

    public function news($_locale, $page)
    {
        $Response['page'] = 'news';

        $AllNews = $this
            ->getEntityManager()
            ->getRepository('WebsiteBackendBundle:News')
            ->findNewsByLocale($_locale);

        $Response['entities'] = $this->getHelperClass()->paginator($AllNews, $page, Data::$limit_page);

        return $Response;
    }

    public function getHelperClass()
    {
        return $this->_Helper_class;
    }

    public function newsSingle($_locale, $slug)
    {
        $Response['page'] = 'news';
        $Response['entity'] = $this->getEntityManager()->getRepository('WebsiteBackendBundle:News')->findOneBySlug($slug);

        $Response['entities'] = $this->getEntityManager()->getRepository('WebsiteBackendBundle:News')->findNewsRecords($_locale, $slug, 3);

        return $Response;
    }

    public function rewiews($type, $page)
    {
        $Entities = $this->getEntityManager()->getRepository('WebsiteBackendBundle:Reviews')->findByTypeAndStatus($type);

        $Form = $this->getComposite()
            ->getFormSugar()
            ->createForm(
                ReviewsType::class,
                new Reviews

            );

        $Response['entities'] = $this->getHelperClass()->paginator($Entities, $page, Data::$limit_page);
        $Response['form'] = $Form->createView();
        $Response['type'] = $type;
        $Response['page'] = 'reviews';

        return $Response;
    }

    /**
     * Get Model
     *
     * @return Profiler\AdminBundle\Services\Composite
     */
    public function getComposite()
    {
        return $this->_Composite;
    }

    public function sendRewiews($request, $User, $type)
    {
        $Entity = new Reviews;

        $Form = $this->getComposite()
            ->getFormSugar()
            ->createForm(
                ReviewsType::class,
                $Entity

            );

        $Form->handleRequest($request);

        if ($Form->isSubmitted() && $Form->isValid()) {
            $Entity->setUserFront($User);

            if ($type == 'ytb') {
                $result = $this->validate($Entity->getVideoUrl());

                if ($result !== false) {
                    $Entity->setVideoUrl($result);

                    $this->getEntityManager()->Persist($Entity);
                    $this->getEntityManager()->Flush();
                }
                return $result;
            }
        }
        $this->dispatchErrorFlash($this->getHelperClass()->Error(Constant::NOT_VALID));
        return false;

    }

    public function validate($videoUrl)
    {
        if (filter_var($videoUrl, FILTER_VALIDATE_URL) == TRUE) {

            /*Определение домена ссылки*/
            $domain = parse_url($videoUrl);

            preg_match_all("/(\w+)/i", $domain["host"], $arr, PREG_PATTERN_ORDER);

            $res = array_reverse($arr[0]);

            $domen = $res[1] . '.' . $res[0];

            if ($domen == 'youtube.com' || $domen == 'youtu.be') {
                /*Достаем адрес видео*/
                preg_match('/https:\/\/(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/watch(?:\?|#\!)v=)([\w-]{11}).*/ ', $videoUrl, $videoId);
                if (count($videoId) > 0) {
                    $this->getHelperClass()->dispatchSuccessFlash($this->getHelperClass()->Success(Constant::SUCCESS_REVIEWS_SAVED));

                    return $videoId[1];
                }

            }
        }
        $this->getHelperClass()->dispatchErrorFlash($this->getHelperClass()->Error(Constant::NOT_VALID));

        return false;
    }

    public function Feedback($Request)
    {
        $form = $this->getComposite()
            ->getFormSugar()
            ->createForm(
                FeedbackType::class,
                new Feedback()
            );

        $form->handleRequest($Request);

        if ($form->isValid()) {
            $params = [
                'template' => 'feedback_email',
                'param' => [
                    'name' => $Request->request->get('feedback')['name'],
                    'email' => $Request->request->get('feedback')['email'],
                    'message' => $Request->request->get('feedback')['message']
                ]
            ];

            $this->getHelperClass()->dispatchSendEmail($params);

            $this->getHelperClass()->dispatchSuccessFlash($this->getHelperClass()->Success(Constant::SUCCESS_EMAIL));

        } else {

            $this->getHelperClass()->dispatchErrorFlash($this->getHelperClass()->Error(Constant::NOT_VALID));
        }

        return $this;
    }

}