<?php

namespace Website\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DoubleAuthType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', TextType::class, array(
                    'label' => 'mess.form.code',
                    'label_attr' => array('class' => 'label margin-bottom-2'),
                    'attr' => array('class' => 'input', 'placeholder' => 'code', 'data-input-mask' => '', 'autocomplete' => 'off'),
                    'trim' => true,
                    'required' => true
                )
            )
            ->add('send', SubmitType::class, array(
                    'label' => 'sign_in',
                    'attr' => array('class' => 'btn btn_alt', 'data-theme' => 'advanced')
                )
            );;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(

            'translation_domain' => 'messages',

        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'frontend_authorization';
    }
}
