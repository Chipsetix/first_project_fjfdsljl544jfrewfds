<?php

namespace Website\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReviewsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('videoUrl', TextType::class,
                array(
                    'label_attr' => array('class' => 'label margin-bottom-2 margin-bottom-0--lg space-nowrap'),
                    'label' => 'front.reviews.link',
                    'attr' => array('class' => 'make-review__link input margin-bottom-3 margin-bottom-0--lg margin-x-3--lg', 'id' => 'user_feedback_message', 'data-theme' => 'advanced', 'placeholder' => 'front.reviews.link'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => true
                )
            )
            ->add('type', HiddenType::class, array(
                'data' => 'ytb',
            ))
            ->add('send', SubmitType::class, array(
                    'label' => 'front.reviews.button',
                    'attr' => array('class' => 'btn btn_alt', 'data-theme' => 'advanced')
                )
            );;
    }


    /**
     * @param configureOptions $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Website\BackendBundle\Entity\Reviews',
            'translation_domain' => 'messages',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'frontbundle_reviews';
    }
}