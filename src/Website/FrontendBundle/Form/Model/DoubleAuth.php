<?php

namespace Website\FrontendBundle\Form\Model;


class DoubleAuth
{

    private $code;


    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }


}
