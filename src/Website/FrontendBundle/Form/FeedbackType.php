<?php

namespace Website\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FeedbackType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,
                array(
                    'translation_domain' => 'messages',
                    'label_attr' => array('class' => 'margin-bottom-1'),
                    'label' => 'guest.contacts.full_name',
                    'attr' => array('class' => 'field', 'maxlength' => '25', 'data-theme' => 'advanced'),
                    'required' => true
                )
            )
            ->add('email', TextType::class,
                array(
                    'translation_domain' => 'messages',
                    'label_attr' => array('class' => 'margin-bottom-1'),
                    'label' => 'guest.contacts.email',
                    'attr' => array('class' => 'field', 'maxlength' => '25', 'data-theme' => 'advanced'),
                    'required' => true
                )
            )
            ->add('message', TextareaType::class,
                array(
                    'translation_domain' => 'messages',
                    'label_attr' => array('class' => 'margin-bottom-1'),
                    'label' => 'guest.contacts.description',
                    'attr' => array('class' => 'field', 'rows' => 6, 'data-theme' => 'advanced'),
                    'required' => true
                )
            );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(

            'translation_domain' => 'UserBundle',

        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'frontend_feedback';
    }
}
