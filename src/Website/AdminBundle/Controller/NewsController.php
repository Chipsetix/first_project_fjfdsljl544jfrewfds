<?php

namespace Website\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NewsController extends Controller
{

    /**
     * @Template("WebsiteAdminBundle:News:index.html.twig")
     */
    public function indexAction($page)
    {
        return $this->get('website_admin.news')->ShowAll($page);
    }

    /**
     * @Template("WebsiteAdminBundle:News:form.html.twig")
     */
    public function newAction()
    {
        return $this->get('website_admin.news')->Novel();
    }


    public function createAction(Request $request)
    {
        return $this->get('website_admin.news')->CreateElement($request);

    }

    /**
     * @Template("WebsiteAdminBundle:News:show.html.twig")
     */
    public function showAction($id)
    {
        return $this->get('website_admin.news')->Show($id);
    }

    /**
     * @Template("WebsiteAdminBundle:News:form.html.twig")
     */
    public function editAction($id)
    {
        return $this->get('website_admin.news')->Edit($id);
    }


    public function updateAction(Request $request, $id)
    {
        return $this->get('website_admin.news')->Update($request, $id);
    }


    public function deleteAction(Request $request, $id)
    {
        return $this->get('website_admin.news')->Delete($request, $id);
    }

}
