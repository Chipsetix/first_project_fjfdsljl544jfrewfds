<?php

namespace Website\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SEOController extends Controller
{

    /**
     * @Template("WebsiteAdminBundle:SEO:index.html.twig")
     */
    public function indexAction()
    {
        return $this->get('website_admin.seo')->ShowAll();
    }

    /**
     * @Template("WebsiteAdminBundle:SEO:form.html.twig")
     */
    public function newAction()
    {
        return $this->get('website_admin.seo')->Novel();
    }

    public function createAction(Request $request)
    {
        return $this->get('website_admin.seo')->CreateElement($request);
    }

    /**
     * @Template("WebsiteAdminBundle:SEO:show.html.twig")
     */
    public function showAction($id)
    {
        return $this->get('website_admin.seo')->Show($id);
    }

    /**
     * @Template("WebsiteAdminBundle:SEO:form.html.twig")
     */
    public function editAction($id)
    {
        return $this->get('website_admin.seo')->Edit($id);
    }

    public function updateAction(Request $request, $id)
    {
        return $this->get('website_admin.seo')->Update($request, $id);
    }

    public function deleteAction(Request $request, $id)
    {
        return $this->get('website_admin.seo')->Delete($request, $id);
    }

}