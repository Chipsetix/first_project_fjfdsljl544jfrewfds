<?php

namespace Website\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FAQController extends Controller
{

    /**
     * @Template("WebsiteAdminBundle:FAQ:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        return $this->get('website_admin.faq')->ShowAll($request);
    }


    /**
     * @Template("WebsiteAdminBundle:FAQ:form.html.twig")
     */
    public function newAction()
    {
        return $this->get('website_admin.faq')->Novel();
    }


    public function createAction(Request $request)
    {
        return $this->get('website_admin.faq')->CreateElement($request);

    }

    /**
     * @Template("WebsiteAdminBundle:FAQ:show.html.twig")
     */
    public function showAction(Request $request, $id)
    {
        return $this->get('website_admin.faq')->Show($request, $id);
    }

    /**
     * @Template("WebsiteAdminBundle:FAQ:form.html.twig")
     */
    public function editAction($id)
    {
        return $this->get('website_admin.faq')->Edit($id);
    }


    public function updateAction(Request $request, $id)
    {
        return $this->get('website_admin.faq')->Update($request, $id);
    }


    public function deleteAction(Request $request, $id)
    {
        return $this->get('website_admin.faq')->Delete($request, $id);
    }

}
