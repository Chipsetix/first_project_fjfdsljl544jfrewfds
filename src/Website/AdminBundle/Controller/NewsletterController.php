<?php

namespace Website\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NewsletterController extends Controller
{

    /**
     * @Template("WebsiteAdminBundle:Newsletter:index.html.twig")
     */
    public function indexAction()
    {
        return $this->get('website_admin.newsletter')->ShowAll();
    }

    /**
     * @Template("WebsiteAdminBundle:Newsletter:form.html.twig")
     */
    public function newAction(Request $request)
    {

        $Response = $this->get('website_admin.newsletter')->Novel();

        return $this->render('WebsiteAdminBundle:Newsletter:form.html.twig',
            array(

                'entity' => $Response['entity'],
                'form' => $Response['form'],
                'UserId' => $request->get('Userid')

            )
        );

    }

    public function createAction(Request $request, $Users = NULL)
    {
        if ($request->request->get('userFront') !== NULL) {

            $em = $this->getDoctrine()->getManager();

            $Users = $em->getRepository('WebsiteBackendBundle:userFrontend')->findOneById($request->request->get('userFront'));
        }

        return $this->get('website_admin.newsletter')->CreateElement($request, $Users);

    }

    /**
     * @Template("WebsiteAdminBundle:Newsletter:show.html.twig")
     */
    public function showAction($id)
    {
        return $this->get('website_admin.newsletter')->Show($id);
    }

    public function editAction(Request $request, $id)
    {
        $Response = $this->get('website_admin.newsletter')->Edit($id);

        return $this->render('WebsiteAdminBundle:Newsletter:form.html.twig',
            array(

                'entity' => $Response['entity'],
                'edit_form' => $Response['edit_form'],
                'UserId' => $request->get('Userid')

            )
        );
    }

    public function updateAction(Request $request, $id)
    {
        return $this->get('website_admin.newsletter')->Update($request, $id);
    }

    public function deleteAction(Request $request, $id)
    {
        return $this->get('website_admin.newsletter')->Delete($request, $id);
    }

}
