<?php

namespace Website\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ReviewsController extends Controller
{

    /**
     * @Template("WebsiteAdminBundle:Reviews:index.html.twig")
     */
    public function indexAction()
    {
        return $this->get('website_admin.reviews')->ShowAll();
    }


    /**
     * @Template("WebsiteAdminBundle:Reviews:form.html.twig")
     */
    public function newAction()
    {
        return $this->get('website_admin.reviews')->Novel();
    }

    /**
     * @Template("WebsiteAdminBundle:Reviews:form.html.twig")
     */
    public function createAction(Request $request)
    {
        return $this->get('website_admin.reviews')->CreateElement($request);

    }

    /**
     * @Template("WebsiteAdminBundle:Reviews:show.html.twig")
     */
    public function showAction($id)
    {
        return $this->get('website_admin.reviews')->Show($id);
    }

    /**
     * @Template("WebsiteAdminBundle:Reviews:form.html.twig")
     */
    public function editAction($id)
    {
        return $this->get('website_admin.reviews')->Edit($id);
    }


    public function updateAction(Request $request, $id)
    {
        return $this->get('website_admin.reviews')->Update($request, $id);
    }


    public function deleteAction(Request $request, $id)
    {
        return $this->get('website_admin.reviews')->Delete($request, $id);
    }

}
