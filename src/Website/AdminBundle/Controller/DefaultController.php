<?php

namespace Website\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('@WebsiteAdmin/Default/index.html.twig');
    }

    public function getNavbarAction()
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('WebsiteAdminBundle::nav_bar.html.twig');
    }


    /**
     *пределяем разницу между датами
     *
     * @param dateTime $date1
     * @param dateTime $date2
     *
     * @return int
     */
    public function date_diff($date1, $date2)
    {
        $diff = (strtotime($date2) - strtotime($date1)) / 3600 / 24;

        return $diff;
    }
}
