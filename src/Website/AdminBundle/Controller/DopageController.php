<?php

namespace Website\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DopageController extends Controller
{

    /**
     * @Template("WebsiteAdminBundle:Dopage:index.html.twig")
     */
    public function indexAction($page)
    {
        return $this->get('website_admin.dopage')->ShowAll($page);
    }

    /**
     * @Template("WebsiteAdminBundle:Dopage:form.html.twig")
     */
    public function newAction()
    {
        return $this->get('website_admin.dopage')->Novel();
    }

    public function createAction(Request $request)
    {
        return $this->get('website_admin.dopage')->CreateElement($request);

    }

    /**
     * @Template("WebsiteAdminBundle:Dopage:show.html.twig")
     */
    public function showAction($id)
    {
        return $this->get('website_admin.dopage')->Show($id);
    }

    /**
     * @Template("WebsiteAdminBundle:Dopage:form.html.twig")
     */
    public function editAction($id)
    {
        return $this->get('website_admin.dopage')->Edit($id);
    }

    public function updateAction(Request $request, $id)
    {
        return $this->get('website_admin.dopage')->Update($request, $id);
    }

    public function deleteAction(Request $request, $id)
    {
        return $this->get('website_admin.dopage')->Delete($request, $id);
    }

}
