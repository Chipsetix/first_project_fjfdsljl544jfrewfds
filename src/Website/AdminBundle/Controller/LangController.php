<?php

namespace Website\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LangController extends Controller
{

    /**
     * @Template("WebsiteAdminBundle:Lang:index.html.twig")
     */
    public function indexAction()
    {
        return $this->get('website_admin.lang')->ShowAll();
    }


    /**
     * @Template("WebsiteAdminBundle:Lang:form.html.twig")
     */
    public function newAction()
    {
        return $this->get('website_admin.lang')->Novel();
    }


    public function createAction(Request $request)
    {
        return $this->get('website_admin.lang')->CreateElement($request);

    }

    /**
     * @Template("WebsiteAdminBundle:Lang:show.html.twig")
     */
    public function showAction($id)
    {
        return $this->get('website_admin.lang')->Show($id);
    }

    /**
     * @Template("WebsiteAdminBundle:Lang:form.html.twig")
     */
    public function editAction($id)
    {
        return $this->get('website_admin.lang')->Edit($id);
    }


    public function updateAction(Request $request, $id)
    {
        return $this->get('website_admin.lang')->Update($request, $id);
    }


    public function deleteAction(Request $request, $id)
    {
        return $this->get('website_admin.lang')->Delete($request, $id);
    }

}
