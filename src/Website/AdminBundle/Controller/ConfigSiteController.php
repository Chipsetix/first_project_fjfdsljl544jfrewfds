<?php

namespace Website\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ConfigSiteController extends Controller
{

    public function showAction()
    {
        $entity = $this->getDoctrine()->getManager()->getRepository('WebsiteBackendBundle:ConfigSite')->findByPage('config-site');

        return $this->render('WebsiteAdminBundle:ConfigSite:show.html.twig', [
            $this->get('admin.site')->Show(),
            'entity' => $entity[0]
        ]);
    }


    public function editAction()
    {
        $response = $this->get('admin.site')->Edit();

        if (is_object($response)) {
            return $response;
        }

        return $this->render('WebsiteAdminBundle:ConfigSite:form.html.twig', [
            'entity' => $response['entity'],
            'form' => $response['edit_form']
        ]);


    }


    public function updateAction(Request $request)
    {
        return $this->get('admin.site')->Update($request);
    }

    public function createAction(Request $request)
    {

        $response = $this->get('admin.site')->CreateElement($request);

        if (is_object($response)) {

            return $response;
        }

        return $this->render('WebsiteAdminBundle:ConfigSite:form.html.twig', [
            'entity' => $response['entity'],
            'form' => $response['form']
        ]);
    }

}