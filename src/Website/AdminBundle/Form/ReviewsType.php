<?php

namespace Website\AdminBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReviewsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userFront', EntityType::class, array(
                'label' => 'Пользователь',
                'label_attr' => array('class' => 'col-sm-2 control-label'),
                'attr' => array('class' => '', 'data-theme' => 'advanced'),
                // looks for choices from this entity
                'class' => 'WebsiteBackendBundle:User',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.id', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'username',
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ))
            ->add('type', ChoiceType::class, array(
                    'choices' => array(
                        'Текст' => 'txt',
                        'Видео' => 'ytb',
                    ),
                    'label' => 'Выберите тип',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('data-theme' => 'advanced'),
                    'expanded' => true,
                    'multiple' => false,
                    'required' => true
                )
            )
            ->add('description', TextareaType::class, array(
                    'label' => 'Отзыв',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => 'Отзыв'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => false
                )
            )
            ->add('videoUrl', TextType::class,
                array(
                    'label' => 'Link Youtube',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => 'id youtube'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => false
                )
            )
            ->add('dateCreate', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => true,
                'label' => 'Дата создания',
                'label_attr' => array('class' => 'col-sm-2 control-label'),
                'attr' => array('class' => 'form-control pull-righ datepicker', 'data-theme' => 'advanced', 'placeholder' => 'dd-mm-yyyy')
            ))
            ->add('status', CheckboxType::class, array(
                    'label' => 'Одобрено',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'minimal form-control'),
                    'required' => false
                )
            )
            ->add('send', SubmitType::class, array(
                    'label' => 'Сохранить',
                    'attr' => array('class' => 'form-control col-sm-2 btn btn-success', 'data-theme' => 'advanced')
                )
            );;

    }


    /**
     * @param configureOptions $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Website\BackendBundle\Entity\Reviews'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_reviews';
    }
}