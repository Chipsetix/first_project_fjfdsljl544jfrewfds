<?php

namespace Website\AdminBundle\Form;

use Doctrine\ORM\EntityRepository;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                    'label' => 'Название',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => 'Название'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => true
                )
            )
            ->add('lang', EntityType::class, array(
                'label' => 'Выберите Язык',
                'label_attr' => array('class' => 'col-sm-2 control-label'),
                'attr' => array('class' => '', 'data-theme' => 'advanced'),
                // looks for choices from this entity
                'class' => 'WebsiteBackendBundle:Lang',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.con_num', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'title',
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ))
            ->add('dateCreate', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => true,
                'label' => 'Дата создания',
                'label_attr' => array('class' => 'col-sm-2 control-label'),
                'attr' => array('class' => 'form-control pull-righ datepicker', 'data-theme' => 'advanced', 'placeholder' => 'dd-mm-yyyy')
            ))
            ->add('description', CKEditorType::class, array(
                    'config_name' => 'my_config',
                    'label' => 'Описание',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => 'Описание'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => true
                )
            )
//            ->add('photo',TextType::class, array(
//                    'label' => 'Ссылка на фото',
//                    'label_attr'=>array('class'=>'col-sm-2 control-label'),
//                    'attr'=>array('class'=>'form-control', 'data-theme' => 'advanced','placeholder'=>'Ссылка на фото'),
//                    'auto_initialize' => false,
//                    'trim'=>true,
//                    'required'=>true
//                )
//            )
            ->add('send', SubmitType::class, array(
                    'label' => 'Сохранить',
                    'attr' => array('class' => 'form-control col-sm-2 btn btn-success', 'data-theme' => 'advanced')
                )
            );;

    }


    /**
     * @param configureOptions $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Website\BackendBundle\Entity\News'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_bundle_news';
    }
}