<?php

namespace Website\AdminBundle\Form;

use Doctrine\ORM\EntityRepository;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FAQType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('question', TextType::class, array(
                    'label' => 'Вопрос',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => 'Вопрос'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => true
                )
            )
            ->add('lang', EntityType::class, array(
                'label' => 'Выберите Язык',
                'label_attr' => array('class' => 'col-sm-2 control-label'),
                'attr' => array('class' => '', 'data-theme' => 'advanced'),
                // looks for choices from this entity
                'class' => 'WebsiteBackendBundle:Lang',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.con_num', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'title',
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ))
            ->add('category', ChoiceType::class, array(
                    'choices' => array(
                        'Общие вопросы' => '1',
                        'Технические вопросы' => '2',
                        'Финансовые вопросы' => '3',
                        'Другие вопросы' => '4',
                    ),
                    'label' => 'Категирия',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => '', 'data-theme' => 'advanced')
                )
            )
            ->add('answer', CKEditorType::class, array(
                    'label' => 'Ответ',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => 'Ответ'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => true
                )
            )
            ->add('send', SubmitType::class, array(
                    'label' => 'Сохранить',
                    'attr' => array('class' => 'form-control col-sm-2 btn btn-success', 'data-theme' => 'advanced')
                )
            );;

    }


    /**
     * @param configureOptions $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Website\BackendBundle\Entity\FAQ'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_faq';
    }
}