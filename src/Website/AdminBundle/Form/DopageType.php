<?php

namespace Website\AdminBundle\Form;

use Doctrine\ORM\EntityRepository;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DopageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titlePathFirst', TextType::class, array(
                    'label' => 'Заголовок 1 часть*',
                    'label_attr' => array('class' => 'control-label col-sm-2'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => 'Заголовок'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => true
                )
            )
            ->add('url', TextType::class, array(
                    'label' => 'Ссылка',
                    'label_attr' => array('class' => 'control-label col-sm-2'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => 'Например - about'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => true
                )
            )
            ->add('lang', EntityType::class, array(
                'label' => 'Выберите Язык',
                'label_attr' => array('class' => 'col-sm-2 control-label'),
                'attr' => array('class' => '', 'data-theme' => 'advanced'),
                'class' => 'WebsiteBackendBundle:Lang',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')->orderBy('u.con_num', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'title',
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ))
            ->add('description', CKEditorType::class, array(
                    'config_name' => 'my_config',
                    'label' => 'Описание',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => 'Описание'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => true
                )
            )
            ->add('send', SubmitType::class, array(
                    'label' => 'Сохранить',
                    'attr' => array('class' => 'form-control col-sm-2 btn btn-success', 'data-theme' => 'advanced')
                )
            );;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(

            'data_class' => 'Website\BackendBundle\Entity\Dopage'

        ));
    }

}
