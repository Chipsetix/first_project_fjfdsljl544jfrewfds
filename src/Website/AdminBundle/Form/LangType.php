<?php

namespace Website\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LangType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                    'label' => 'Язык (название)',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => 'название'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => true
                )
            )
            ->add('slug', TextType::class, array(
                    'label' => 'Slug',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => 'Russia'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => false
                )
            )
            ->add('locale', TextType::class, array(
                    'label' => 'Префикс',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => 'Пример ru,en'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => true
                )
            )
            ->add('disable', CheckboxType::class, array(
                    'label' => 'Не отображать на сайте',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'minimal form-control'),
                    'required' => false
                )
            )
            ->add('con_num', NumberType::class, array(
                    'label' => 'Порядковый номер',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => 'Пример 1,2'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => true
                )
            )
            ->add('send', SubmitType::class, array(
                    'label' => 'Сохранить',
                    'attr' => array('class' => 'form-control col-sm-2 btn btn-success', 'data-theme' => 'advanced')
                )
            );;

    }


    /**
     * @param configureOptions $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Website\BackendBundle\Entity\Lang'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_lang';
    }
}