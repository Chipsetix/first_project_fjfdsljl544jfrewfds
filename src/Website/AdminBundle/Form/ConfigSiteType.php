<?php

namespace Website\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

#use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConfigSiteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('baseDir', TextType::class, array(
                    'label' => 'Базовый путь',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => '/www/website/'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => false
                )
            )
            ->add('domain', TextType::class, array(
                    'label' => 'Домен',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => 'nika.com'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => false
                )
            )
            ->add('countries', TextType::class, array(
                    'label' => 'Количество стран',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => '10'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => false
                )
            )
            ->add('users', TextType::class, array(
                    'label' => 'Количество пользователей',
                    'label_attr' => array('class' => 'col-sm-2 control-label'),
                    'attr' => array('class' => 'form-control', 'data-theme' => 'advanced', 'placeholder' => '20000'),
                    'auto_initialize' => false,
                    'trim' => true,
                    'required' => false
                )
            )
            ->add('send', SubmitType::class, array(
                    'label' => 'Сохранить',
                    'attr' => array('class' => 'form-control col-sm-2 btn btn-success', 'data-theme' => 'advanced')
                )

            );;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(

            'data_class' => 'Website\BackendBundle\Entity\ConfigSite'

        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_configsite';
    }
}