<?php

namespace Website\AdminBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Website\AdminBundle\Form\SEOType as Form;
use Website\BackendBundle\Entity\SEO as Entity;
use Website\BackendBundle\Services\Composite;

Class SEODecorator
{
    /**
     * Model class
     *
     * @var Model
     */
    private $_Composite;

    /**
     * Table name
     *
     * @var string
     */
    private $_table_name;


    /**
     * @param Composite $Composite
     * @param string $tableName
     */
    public function __construct(Composite $Composite, $tableName)
    {
        $this->_Composite = $Composite;

        $this->_table_name = $tableName;

        #init repos
        $this
            ->getComposite()
            ->getEntityManagerSugar()
            ->setTableName($this->getTableName());
    }

    /**
     * Get Model
     *
     * @return Profiler\AdminBundle\Services\Composite
     */
    public function getComposite()
    {
        return $this->_Composite;
    }


    /**
     * Returns table name
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->_table_name;
    }

    /**
     * Show All rows
     *
     * @return Array
     */
    public function ShowAll()
    {
        return $this->getComposite()->ShowAll();

    }

    /**
     * Returns array for render view "Create new element"
     *
     * @return Array
     */
    public function Novel()
    {
        return $this
            ->getComposite()
            ->Novel(
                new Entity(),
                Form::class
            );
    }


    /**
     * Create element in DataBase and redirect
     * @param Request $Request
     *
     * @return RedirectResponse
     */
    public function CreateElement(Request $Request)
    {
        return $this
            ->getComposite()
            ->CreateElement(
                $Request,
                new Entity(),
                Form::class,
                'admin_seo_show'
            );
    }

    /**
     * Show one row by id
     *
     * @param int $id
     *
     * @return Array
     */
    public function Show($id)
    {
        return $this->getComposite()->Show($id);

    }


    /**
     * Returns array for render view "Edit element"
     *
     * @param int $id
     *
     * @return Array
     */
    public function Edit($id)
    {
        return $this->getComposite()->Edit($id, Form::class);

    }


    /**
     * Update element and redirect
     *
     * @param Request $Request
     *
     * @return RedirectResponse
     */
    public function Update(Request $Request, $id)
    {

        return $this
            ->getComposite()
            ->Update(
                $Request,
                $id,
                Form::class,
                'admin_seo_show',
                array('id' => $id)
            );

    }

    /**
     * Delete object adn redirect
     *
     * @param Request $request
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function Delete(Request $request, $id)
    {
        return $this->getComposite()->Delete($request, $id, 'admin_seo');
    }

}