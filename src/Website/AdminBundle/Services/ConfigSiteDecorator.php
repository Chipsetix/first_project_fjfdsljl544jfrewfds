<?php

namespace Website\AdminBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Website\AdminBundle\Form\ConfigSiteType as Form;
use Website\BackendBundle\Entity\ConfigSite as ConfigSite;
use Website\BackendBundle\Services\Composite;

Class ConfigSiteDecorator
{
    /**
     * Model class
     *
     * @var Model
     */
    private $_Composite;

    /**
     * Table name
     *
     * @var string
     */
    private $_table_name;


    /**
     * @param Composite $Composite
     * @param string $tableName
     */
    public function __construct(Composite $Composite, $tableName)
    {
        $this->_Composite = $Composite;

        $this->_table_name = $tableName;

        #init repos
        $this
            ->getComposite()
            ->getEntityManagerSugar()
            ->setTableName($this->getTableName());
    }

    /**
     * Get Model
     *
     * @return Profiler\AdminBundle\Services\Composite
     */
    public function getComposite()
    {
        return $this->_Composite;
    }


    /**
     * Returns table name
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->_table_name;
    }

    /**
     * Create element in DataBase and redirect
     * @TODO добавить проверку на существование. Если есть перекидывать на Update
     * @param Request $Request
     *
     * @return RedirectResponse
     */
    public function CreateElement(Request $Request)
    {
        $entity = new ConfigSite();
        $form = Form::class;
        $route = 'site_show';

        return $this
            ->getComposite()
            ->CreateElement($Request, $entity, $form, $route, false);
    }

    /**
     * Show one row
     *
     * @return Array
     */
    public function Show()
    {
        return array(
            'entity' => $this->getConfigSite()
        );
    }

    /**
     * @return this
     */
    public function getConfigSite()
    {
        return $this
            ->getComposite()
            ->getEntityManagerSugar()
            ->getRepository('WebsiteBackendBundle:ConfigSite')
            ->findInfo();
    }

    /**
     * Returns array for render view "Edit element"
     *
     * @return Array
     */
    public function Edit()
    {
        $Entity = $this->getConfigSite();
        $form = Form::class;

        return array(
            'entity' => $Entity,
            'edit_form' => $this
                ->getComposite()
                ->getFormSugar()
                ->createEditForm($form, $Entity)
                ->createView(),
        );
    }


    /**
     * Update element and redirect
     *
     * @param Request $Request
     *
     * @return RedirectResponse
     */
    public function Update(Request $Request)
    {
        $Entity = $this->getConfigSite();

        $EditForm = $this
            ->getComposite()
            ->getFormSugar()
            ->createEditForm(Form::class, $Entity);

        $EditForm->handleRequest($Request);

        if ($EditForm->isValid()) {
            $this
                ->getComposite()
                ->getEntityManagerSugar()
                ->PersistAndFlush($Entity);
            $this
                ->getComposite()
                ->FlashBag('success', 'Запись успешно cохранена!');
        }
//
        return $this
            ->getComposite()
            ->getSupport()
            ->MakeRedirect('site_show', NULL);

    }

}