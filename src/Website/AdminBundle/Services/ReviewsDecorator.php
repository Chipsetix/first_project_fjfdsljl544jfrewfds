<?php

namespace Website\AdminBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Website\AdminBundle\Form\ReviewsType as Form;
use Website\BackendBundle\Entity\Reviews as Entity;
use Website\BackendBundle\Services\Composite;

#implements ServiceModelForEntityInterface
# ActionInterface
Class ReviewsDecorator
{
    /**
     * Model class
     *
     * @var Model
     */
    private $_Composite;

    /**
     * Table name
     *
     * @var string
     */
    private $_table_name;

    /**
     * @param Composite $Composite
     * @param string $tableName
     */
    public function __construct(Composite $Composite, $tableName)
    {
        $this->_Composite = $Composite;

        $this->_table_name = $tableName;

        #init repos
        $this
            ->getComposite()
            ->getEntityManagerSugar()
            ->setTableName($this->getTableName());

    }


    /**
     * Get Model
     *
     * @return Website\AdminBundle\Services\Model
     */
    public function getComposite()
    {
        return $this->_Composite;
    }


    /**
     * Returns table name
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->_table_name;
    }

    /**
     * Show All rows
     *
     * @return Array
     */
    public function ShowAll()
    {
        return $this->getComposite()->ShowAll();

    }

    /**
     * Returns array for render view "Create new element"
     *
     * @return Array
     */
    public function Novel()
    {

        return $this->getComposite()->Novel(new Entity(), Form::class);

    }


    /**
     * Create element in DataBase and redirect
     *
     * @param Request $Request
     *
     * @return RedirectResponse
     */
    public function CreateElement(Request $Request)
    {
        $Entity = new Entity;

        $Form = $this->getComposite()
            ->getFormSugar()
            ->createForm(
                Form::class,
                $Entity

            );

        $Form->handleRequest($Request);


        if ($Form->isValid()) {
            $type = $Request->request->get('reviews')['type'];

            if ($type == 'txt') {
                $Response = $this->textReviews($Entity);

            } else {

                $Response = $this->videoReviews($Entity);

            }

            if (is_string($Response)) {

                $this->getComposite()->FlashBag('notice', $Response);

            } else {

                $this->getComposite()->getEntityManagerSugar()->PersistAndFlush($Entity);

                $this->getComposite()->FlashBag('success', 'Запись успешно cохранена!');

                return $this
                    ->getComposite()
                    ->getSupport()
                    ->MakeRedirect(
                        'admin_reviews_show',
                        array('id' => $Entity->getId())
                    );
            }
        }
        return array(
            'entity' => $Entity,
            'form' => $this
                ->getFromErrorElement(
                    $Request
                )
                ->createView()
        );

    }

    public function textReviews($Entity)
    {

        $value = trim($Entity->getDescription());

        if (preg_match('/^(http:\/\/|https:\/\/)?([^\.\/]+\.)*([a-zA-Z0-9])([a-zA-Z0-9-]*)\.([a-zA-Z]{2,4})(\/.*)?$/i', $value)) {
            return 'Не должно содержать ссылки!';

        } else {
            return $Entity;
        }
    }

    public function videoReviews($Entity)
    {
        $videoUrl = $Entity->getVideoUrl();

        /*Определение домена ссылки*/
        $domain = parse_url($videoUrl);

        preg_match_all("/(\w+)/i", $domain["host"], $arr, PREG_PATTERN_ORDER);

        $res = array_reverse($arr[0]);

        $domen = $res[1] . '.' . $res[0];

        if ($domen == 'youtube.com' || $domen == 'youtu.be') {
            /*Достаем адрес видео*/
            preg_match('/https:\/\/(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/watch(?:\?|#\!)v=)([\w-]{11}).*/ ', $videoUrl, $videoId);
            if (count($videoId) > 0) {

                $Entity->setVideoUrl($videoId[1]);

                return $Entity;
            }

        } else {

            return 'Не является youtube ссылкой!';
        }
    }

    public function getFromErrorElement(Request $Request)
    {
        return $this
            ->getComposite()
            ->ErrorElement(
                $Request,
                new Entity(),
                Form::class
            );

    }

    /**
     * Show one row by id
     *
     * @param int $id
     *
     * @return Array
     */
    public function Show($id)
    {
        return $this->getComposite()->Show($id);

    }

    /**
     * Returns array for render view "Edit element"
     *
     * @param int $id
     *
     * @return Array
     */
    public function Edit($id)
    {
        return $this->getComposite()->Edit($id, Form::class);
    }

    /**
     * Update element and redirect
     *
     * @param Request $Request
     * @param int $id
     * @return RedirectResponse
     */
    public function Update(Request $Request, $id)
    {
//        $Entity=$this->getComposite()->getEntityManagerSugar()->findOne( $id );
//
//        $Form = $this->getComposite()
//            ->getFormSugar()
//            ->createForm(
//                Form::class,
//                $Entity
//
//            );
//
//        $Form->handleRequest( $Request );
//
//
//        if ( $Form->isValid())
//        {
//
//        }
//        dump($Form->createView());die;
        return $this
            ->getComposite()
            ->Update(
                $Request,
                $id,
                Form::class,
                'admin_reviews_show',
                array('id' => $id)
            );

    }

    /**
     * Delete object adn redirect
     *
     * @param Request $request
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function Delete(Request $request, $id)
    {

        return $this->getComposite()->Delete($request, $id, 'admin_reviews');

    }
}