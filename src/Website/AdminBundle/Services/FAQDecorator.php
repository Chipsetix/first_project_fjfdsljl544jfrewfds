<?php

namespace Website\AdminBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Website\AdminBundle\Form\FAQType as Form;
use Website\BackendBundle\Entity\FAQ as Entity;
use Website\BackendBundle\Services\Composite;

#implements ServiceModelForEntityInterface
# ActionInterface
Class FAQDecorator
{
    /**
     * Model class
     *
     * @var Model
     */
    private $_Composite;

    /**
     * Table name
     *
     * @var string
     */
    private $_table_name;

    /**
     * @param Composite $Composite
     * @param string $tableName
     */
    public function __construct(Composite $Composite, $tableName)
    {
        $this->_Composite = $Composite;

        $this->_table_name = $tableName;

        #init repos
        $this
            ->getComposite()
            ->getEntityManagerSugar()
            ->setTableName($this->getTableName());

    }


    /**
     * Get Model
     *
     * @return Website\AdminBundle\Services\Model
     */
    public function getComposite()
    {
        return $this->_Composite;
    }


    /**
     * Returns table name
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->_table_name;
    }

    /**
     * Show All rows
     *
     * @return Array
     */
    public function ShowAll($request)
    {
        $Response = $this->getComposite()->ShowAll();

        $Response['langs'] = $this
            ->getComposite()
            ->getEntityManagerSugar()
            ->getRepository('WebsiteBackendBundle:Lang')
            ->findAll();
        $Response['tab'] = $request->query->get('tab');
        return $Response;
    }

    /**
     * Returns array for render view "Create new element"
     *
     * @return Array
     */
    public function Novel()
    {

        return $this->getComposite()->Novel(new Entity(), Form::class);

    }


    /**
     * Create element in DataBase and redirect
     *
     * @param Request $Request
     *
     * @return RedirectResponse
     */
    public function CreateElement(Request $Request)
    {

        return $this
            ->getComposite()
            ->CreateElement(
                $Request,
                new Entity,
                Form::class,
                'admin_faq_show'
            );

    }

    /**
     * Show one row by id
     *
     * @param int $id
     *
     * @return Array
     */
    public function Show($request, $id)
    {
        $Response = $this->getComposite()->Show($id);

        $Response['tab'] = $request->query->get('tab');

        return $Response;
    }

    /**
     * Returns array for render view "Edit element"
     *
     * @param int $id
     *
     * @return Array
     */
    public function Edit($id)
    {
        return $this->getComposite()->Edit($id, Form::class);
    }

    /**
     * Update element and redirect
     *
     * @param Request $Request
     * @param int $id
     * @return RedirectResponse
     */
    public function Update(Request $Request, $id)
    {
        return $this
            ->getComposite()
            ->Update(
                $Request,
                $id,
                Form::class,
                'admin_faq_show',
                array('id' => $id)
            );

    }

    /**
     * Delete object adn redirect
     *
     * @param Request $request
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function Delete(Request $request, $id)
    {

        return $this->getComposite()->Delete($request, $id, 'admin_faq');

    }

    public function getFromErrorElement(Request $Request)
    {
        return $this
            ->getComposite()
            ->ErrorElement(
                $Request,
                new Entity(),
                Form::class
            );

    }

}