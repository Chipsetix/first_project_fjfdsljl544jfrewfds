<?php

namespace Website\AdminBundle\Services;

use Website\BackendBundle\Services\Composite;

#implements ServiceModelForEntityInterface
# ActionInterface
Class HistoryDecorator
{
    /**
     * Model class
     *
     * @var Model
     */
    private $_Composite;

    private $_Helper_class;

    /**
     * @param Composite $Composite
     */
    public function __construct(Composite $Composite, $HelperClass)
    {
        $this->_Composite = $Composite;

        $this->_Helper_class = $HelperClass;
    }

    /**
     * Get Model
     *
     * @return Website\AdminBundle\Services\Model
     */
    public function getComposite()
    {
        return $this->_Composite;
    }


    /**
     * Returns table name
     *
     * @return string
     */
    public function getHelperClass()
    {
        return $this->_Helper_class;
    }

}