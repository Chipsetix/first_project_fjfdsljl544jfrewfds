var collectionHolder = $('ul#position_list');

var $addTagLink = $('<a href="#" class="btn btn-info">Добавить</a>');

var $newLinkLi = $('<li></li>').append($addTagLink);

var rmForm = '<a href="#" class="remove-button">Удалить</a>';

jQuery(document).ready(function () {

    collectionHolder.append($newLinkLi);

    collectionHolder.data('index', collectionHolder.find(':input').length);

    RemoveTagForm(collectionHolder);

    $addTagLink.on('click', function (e) {
        e.preventDefault();
        addTagForm(collectionHolder, $newLinkLi);
    });

});


function addTagForm(collectionHolder, $newLinkLi) {

    var prototype = collectionHolder.data('prototype');

    var index = collectionHolder.data('index');

    var newForm = prototype.replace(/__name__/g, index);

    collectionHolder.data('index', index + 1);

    var $newFormLi = $('<li class="new_line"></li>').append(newForm);

    $newLinkLi.before($newFormLi);

    addTagFormDeleteLink($newFormLi);
}


function addTagFormDeleteLink($tagFormLi) {

    $tagFormLi.prepend(rmForm).find('.remove-button').on('click', function (e) {

        e.preventDefault();

        $(this).parent().remove();

    });

}


function RemoveTagForm(collectionHolder) {

    var prototype = collectionHolder.data('prototype');

    var index = collectionHolder.data('index');

    var exForm = $('#position_list li:lt(' + index + ')');

    exForm.each(function (i, el) {
        addTagFormDeleteLink($(el));

    });
}
