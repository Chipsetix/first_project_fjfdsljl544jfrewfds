<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitbb39808569e033facab47bf895925075
{
    public static $files = array (
        '92c8763cd6170fce6fcfe7e26b4e8c10' => __DIR__ . '/..' . '/symfony/phpunit-bridge/bootstrap.php',
        '0e6d7bf4a5811bfa5cf40c5ccd6fae6a' => __DIR__ . '/..' . '/symfony/polyfill-mbstring/bootstrap.php',
        '2c102faa651ef8ea5874edb585946bce' => __DIR__ . '/..' . '/swiftmailer/swiftmailer/lib/swift_required.php',
        '5255c38a0faeba867671b61dfda6d864' => __DIR__ . '/..' . '/paragonie/random_compat/lib/random.php',
        'a9ed0d27b5a698798a89181429f162c5' => __DIR__ . '/..' . '/khanamiryan/qrcode-detector-decoder/lib/Common/customFunctions.php',
        'ce89ac35a6c330c55f4710717db9ff78' => __DIR__ . '/..' . '/kriswallsmith/assetic/src/functions.php',
        '32dcc8afd4335739640db7d200c1971d' => __DIR__ . '/..' . '/symfony/polyfill-apcu/bootstrap.php',
        '320cde22f66dd4f5d3fd621d3e88b98f' => __DIR__ . '/..' . '/symfony/polyfill-ctype/bootstrap.php',
        '6a47392539ca2329373e0d33e1dba053' => __DIR__ . '/..' . '/symfony/polyfill-intl-icu/bootstrap.php',
        'bd9634f2d41831496de0d3dfe4c94881' => __DIR__ . '/..' . '/symfony/polyfill-php56/bootstrap.php',
        '023d27dca8066ef29e6739335ea73bad' => __DIR__ . '/..' . '/symfony/polyfill-php70/bootstrap.php',
        '9c220672151e349cf720fdb0422eef0e' => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Component/VarDumper/Resources/functions/dump.php',
    );

    public static $prefixLengthsPsr4 = array (
        'Z' => 
        array (
            'Zxing\\' => 6,
        ),
        'T' => 
        array (
            'Twig\\' => 5,
            'Tests\\' => 6,
        ),
        'S' => 
        array (
            'Symfony\\Polyfill\\Util\\' => 22,
            'Symfony\\Polyfill\\Php70\\' => 23,
            'Symfony\\Polyfill\\Php56\\' => 23,
            'Symfony\\Polyfill\\Mbstring\\' => 26,
            'Symfony\\Polyfill\\Ctype\\' => 23,
            'Symfony\\Polyfill\\Apcu\\' => 22,
            'Symfony\\Component\\' => 18,
            'Symfony\\Bundle\\SwiftmailerBundle\\' => 33,
            'Symfony\\Bundle\\MonologBundle\\' => 29,
            'Symfony\\Bundle\\AsseticBundle\\' => 29,
            'Symfony\\Bundle\\' => 15,
            'Symfony\\Bridge\\Twig\\' => 20,
            'Symfony\\Bridge\\ProxyManager\\' => 28,
            'Symfony\\Bridge\\PhpUnit\\' => 23,
            'Symfony\\Bridge\\Monolog\\' => 23,
            'Symfony\\Bridge\\Doctrine\\' => 24,
            'Stof\\DoctrineExtensionsBundle\\' => 30,
            'Sensio\\Bundle\\GeneratorBundle\\' => 30,
            'Sensio\\Bundle\\FrameworkExtraBundle\\' => 35,
            'Sensio\\Bundle\\DistributionBundle\\' => 33,
        ),
        'P' => 
        array (
            'Psr\\SimpleCache\\' => 16,
            'Psr\\Log\\' => 8,
            'Psr\\Link\\' => 9,
            'Psr\\Container\\' => 14,
            'Psr\\Cache\\' => 10,
            'Patchwork\\' => 10,
        ),
        'M' => 
        array (
            'MyCLabs\\Enum\\' => 13,
            'Monolog\\' => 8,
        ),
        'L' => 
        array (
            'Leafo\\ScssPhp\\' => 14,
        ),
        'K' => 
        array (
            'Knp\\Snappy\\' => 11,
            'Knp\\Bundle\\SnappyBundle\\' => 24,
            'Knp\\Bundle\\PaginatorBundle\\' => 27,
        ),
        'I' => 
        array (
            'Ivory\\JsonBuilder\\' => 18,
            'Ivory\\CKEditorBundle\\' => 21,
            'Incenteev\\ParameterHandler\\' => 27,
        ),
        'G' => 
        array (
            'Gedmo\\' => 6,
        ),
        'F' => 
        array (
            'FontLib\\' => 8,
            'Fig\\Link\\' => 9,
            'FOS\\UserBundle\\' => 15,
        ),
        'E' => 
        array (
            'Endroid\\QrCode\\' => 15,
            'Endroid\\Installer\\' => 18,
        ),
        'D' => 
        array (
            'Dompdf\\' => 7,
            'Doctrine\\Instantiator\\' => 22,
            'Doctrine\\Common\\Inflector\\' => 26,
            'Doctrine\\Common\\Cache\\' => 22,
            'Doctrine\\Common\\Annotations\\' => 28,
            'Doctrine\\Common\\' => 16,
            'Doctrine\\Bundle\\DoctrineCacheBundle\\' => 36,
            'Doctrine\\Bundle\\DoctrineBundle\\' => 31,
            'DASPRiD\\Enum\\' => 13,
        ),
        'C' => 
        array (
            'Composer\\CaBundle\\' => 18,
        ),
        'B' => 
        array (
            'BaconQrCode\\' => 12,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Zxing\\' => 
        array (
            0 => __DIR__ . '/..' . '/khanamiryan/qrcode-detector-decoder/lib',
        ),
        'Twig\\' => 
        array (
            0 => __DIR__ . '/..' . '/twig/twig/src',
        ),
        'Tests\\' => 
        array (
            0 => __DIR__ . '/../..' . '/tests',
        ),
        'Symfony\\Polyfill\\Util\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-util',
        ),
        'Symfony\\Polyfill\\Php70\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-php70',
        ),
        'Symfony\\Polyfill\\Php56\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-php56',
        ),
        'Symfony\\Polyfill\\Mbstring\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-mbstring',
        ),
        'Symfony\\Polyfill\\Ctype\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-ctype',
        ),
        'Symfony\\Polyfill\\Apcu\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-apcu',
        ),
        'Symfony\\Component\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Component',
        ),
        'Symfony\\Bundle\\SwiftmailerBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/swiftmailer-bundle',
        ),
        'Symfony\\Bundle\\MonologBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/monolog-bundle',
        ),
        'Symfony\\Bundle\\AsseticBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/assetic-bundle',
        ),
        'Symfony\\Bundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Bundle',
        ),
        'Symfony\\Bridge\\Twig\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Bridge/Twig',
        ),
        'Symfony\\Bridge\\ProxyManager\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Bridge/ProxyManager',
        ),
        'Symfony\\Bridge\\PhpUnit\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/phpunit-bridge',
        ),
        'Symfony\\Bridge\\Monolog\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Bridge/Monolog',
        ),
        'Symfony\\Bridge\\Doctrine\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Bridge/Doctrine',
        ),
        'Stof\\DoctrineExtensionsBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/stof/doctrine-extensions-bundle',
        ),
        'Sensio\\Bundle\\GeneratorBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/sensio/generator-bundle',
        ),
        'Sensio\\Bundle\\FrameworkExtraBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/sensio/framework-extra-bundle',
        ),
        'Sensio\\Bundle\\DistributionBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/sensio/distribution-bundle',
        ),
        'Psr\\SimpleCache\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/simple-cache/src',
        ),
        'Psr\\Log\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/log/Psr/Log',
        ),
        'Psr\\Link\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/link/src',
        ),
        'Psr\\Container\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/container/src',
        ),
        'Psr\\Cache\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/cache/src',
        ),
        'Patchwork\\' => 
        array (
            0 => __DIR__ . '/..' . '/patchwork/jsqueeze/src',
        ),
        'MyCLabs\\Enum\\' => 
        array (
            0 => __DIR__ . '/..' . '/myclabs/php-enum/src',
        ),
        'Monolog\\' => 
        array (
            0 => __DIR__ . '/..' . '/monolog/monolog/src/Monolog',
        ),
        'Leafo\\ScssPhp\\' => 
        array (
            0 => __DIR__ . '/..' . '/leafo/scssphp/src',
        ),
        'Knp\\Snappy\\' => 
        array (
            0 => __DIR__ . '/..' . '/knplabs/knp-snappy/src/Knp/Snappy',
        ),
        'Knp\\Bundle\\SnappyBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/knplabs/knp-snappy-bundle',
        ),
        'Knp\\Bundle\\PaginatorBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/knplabs/knp-paginator-bundle',
        ),
        'Ivory\\JsonBuilder\\' => 
        array (
            0 => __DIR__ . '/..' . '/egeloen/json-builder/src',
        ),
        'Ivory\\CKEditorBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/egeloen/ckeditor-bundle',
        ),
        'Incenteev\\ParameterHandler\\' => 
        array (
            0 => __DIR__ . '/..' . '/incenteev/composer-parameter-handler',
        ),
        'Gedmo\\' => 
        array (
            0 => __DIR__ . '/..' . '/gedmo/doctrine-extensions/lib/Gedmo',
        ),
        'FontLib\\' => 
        array (
            0 => __DIR__ . '/..' . '/phenx/php-font-lib/src/FontLib',
        ),
        'Fig\\Link\\' => 
        array (
            0 => __DIR__ . '/..' . '/fig/link-util/src',
        ),
        'FOS\\UserBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/friendsofsymfony/user-bundle',
        ),
        'Endroid\\QrCode\\' => 
        array (
            0 => __DIR__ . '/..' . '/endroid/qr-code/src',
        ),
        'Endroid\\Installer\\' => 
        array (
            0 => __DIR__ . '/..' . '/endroid/installer/src',
        ),
        'Dompdf\\' => 
        array (
            0 => __DIR__ . '/..' . '/dompdf/dompdf/src',
        ),
        'Doctrine\\Instantiator\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/instantiator/src/Doctrine/Instantiator',
        ),
        'Doctrine\\Common\\Inflector\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/inflector/lib/Doctrine/Common/Inflector',
        ),
        'Doctrine\\Common\\Cache\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/cache/lib/Doctrine/Common/Cache',
        ),
        'Doctrine\\Common\\Annotations\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/annotations/lib/Doctrine/Common/Annotations',
        ),
        'Doctrine\\Common\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/common/lib/Doctrine/Common',
        ),
        'Doctrine\\Bundle\\DoctrineCacheBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/doctrine-cache-bundle',
        ),
        'Doctrine\\Bundle\\DoctrineBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/doctrine-bundle',
        ),
        'DASPRiD\\Enum\\' => 
        array (
            0 => __DIR__ . '/..' . '/dasprid/enum/src',
        ),
        'Composer\\CaBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/composer/ca-bundle/src',
        ),
        'BaconQrCode\\' => 
        array (
            0 => __DIR__ . '/..' . '/bacon/bacon-qr-code/src',
        ),
    );

    public static $fallbackDirsPsr4 = array (
        0 => __DIR__ . '/../..' . '/src',
    );

    public static $prefixesPsr0 = array (
        'T' => 
        array (
            'Twig_' => 
            array (
                0 => __DIR__ . '/..' . '/twig/twig/lib',
            ),
        ),
        'S' => 
        array (
            'Svg\\' => 
            array (
                0 => __DIR__ . '/..' . '/phenx/php-svg-lib/src',
            ),
            'SensioLabs\\Security' => 
            array (
                0 => __DIR__ . '/..' . '/sensiolabs/security-checker',
            ),
            'Sabberworm\\CSS' => 
            array (
                0 => __DIR__ . '/..' . '/sabberworm/php-css-parser/lib',
            ),
        ),
        'K' => 
        array (
            'Knp\\Component' => 
            array (
                0 => __DIR__ . '/..' . '/knplabs/knp-components/src',
            ),
        ),
        'D' => 
        array (
            'Doctrine\\ORM\\' => 
            array (
                0 => __DIR__ . '/..' . '/doctrine/orm/lib',
            ),
            'Doctrine\\DBAL\\' => 
            array (
                0 => __DIR__ . '/..' . '/doctrine/dbal/lib',
            ),
            'Doctrine\\Common\\Lexer\\' => 
            array (
                0 => __DIR__ . '/..' . '/doctrine/lexer/lib',
            ),
            'Doctrine\\Common\\Collections\\' => 
            array (
                0 => __DIR__ . '/..' . '/doctrine/collections/lib',
            ),
        ),
        'B' => 
        array (
            'Behat\\Transliterator' => 
            array (
                0 => __DIR__ . '/..' . '/behat/transliterator/src',
            ),
        ),
        'A' => 
        array (
            'Assetic' => 
            array (
                0 => __DIR__ . '/..' . '/kriswallsmith/assetic/src',
            ),
        ),
    );

    public static $classMap = array (
        'AppCache' => __DIR__ . '/../..' . '/app/AppCache.php',
        'AppKernel' => __DIR__ . '/../..' . '/app/AppKernel.php',
        'ArithmeticError' => __DIR__ . '/..' . '/symfony/polyfill-php70/Resources/stubs/ArithmeticError.php',
        'AssertionError' => __DIR__ . '/..' . '/symfony/polyfill-php70/Resources/stubs/AssertionError.php',
        'Collator' => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Component/Intl/Resources/stubs/Collator.php',
        'Cpdf' => __DIR__ . '/..' . '/dompdf/dompdf/lib/Cpdf.php',
        'DivisionByZeroError' => __DIR__ . '/..' . '/symfony/polyfill-php70/Resources/stubs/DivisionByZeroError.php',
        'Error' => __DIR__ . '/..' . '/symfony/polyfill-php70/Resources/stubs/Error.php',
        'HTML5_Data' => __DIR__ . '/..' . '/dompdf/dompdf/lib/html5lib/Data.php',
        'HTML5_InputStream' => __DIR__ . '/..' . '/dompdf/dompdf/lib/html5lib/InputStream.php',
        'HTML5_Parser' => __DIR__ . '/..' . '/dompdf/dompdf/lib/html5lib/Parser.php',
        'HTML5_Tokenizer' => __DIR__ . '/..' . '/dompdf/dompdf/lib/html5lib/Tokenizer.php',
        'HTML5_TreeBuilder' => __DIR__ . '/..' . '/dompdf/dompdf/lib/html5lib/TreeBuilder.php',
        'IntlDateFormatter' => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Component/Intl/Resources/stubs/IntlDateFormatter.php',
        'Locale' => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Component/Intl/Resources/stubs/Locale.php',
        'NumberFormatter' => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Component/Intl/Resources/stubs/NumberFormatter.php',
        'ParseError' => __DIR__ . '/..' . '/symfony/polyfill-php70/Resources/stubs/ParseError.php',
        'SessionUpdateTimestampHandlerInterface' => __DIR__ . '/..' . '/symfony/polyfill-php70/Resources/stubs/SessionUpdateTimestampHandlerInterface.php',
        'SqlFormatter' => __DIR__ . '/..' . '/jdorn/sql-formatter/lib/SqlFormatter.php',
        'TypeError' => __DIR__ . '/..' . '/symfony/polyfill-php70/Resources/stubs/TypeError.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitbb39808569e033facab47bf895925075::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitbb39808569e033facab47bf895925075::$prefixDirsPsr4;
            $loader->fallbackDirsPsr4 = ComposerStaticInitbb39808569e033facab47bf895925075::$fallbackDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInitbb39808569e033facab47bf895925075::$prefixesPsr0;
            $loader->classMap = ComposerStaticInitbb39808569e033facab47bf895925075::$classMap;

        }, null, ClassLoader::class);
    }
}
